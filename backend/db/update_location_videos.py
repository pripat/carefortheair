from models import Location, Bill, Charity, app, db

#!/usr/bin/python

# This sample executes a search request for the specified search term.
# Sample usage:
#   python geolocation_search.py --q=surfing --location-"37.42307,-122.08427" --location-radius=50km --max-results=10
# NOTE: To use the sample, you must provide a developer key obtained
#       in the Google APIs Console. Search for "REPLACE_ME" in this code
#       to find the correct place to provide that key..

import argparse

from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


# Set DEVELOPER_KEY to the API key value from the APIs & auth > Registered apps
# tab of
#   https://cloud.google.com/console
# Please ensure that you have enabled the YouTube Data API for your project.
DEVELOPER_KEY = "AIzaSyDX4M2XCF8OInSZY-jG1ev49JAaJDJ3C28"
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"


def get_location_video(city, state):
    try:
        youtube = build("youtube", "v3", developerKey=DEVELOPER_KEY)

        query_string = "{} {} tour".format(city, state)

        # Call the search.list method to retrieve results matching the specified
        # query term.
        search_response = (
            youtube.search()
            .list(
                q=query_string,
                type="video",
                part="id,snippet",
                maxResults=1,
                safeSearch="strict",
                regionCode="us",
                videoEmbeddable="true",
                videoCategoryId="19",  # category id for travel & events
                order="viewCount",
            )
            .execute()
        )

        return search_response["items"][0]["id"]["videoId"]
    except HttpError:
        return None
    except IndexError:
        return None


# Search for new videos for locations that don't have them
# locations = Location.query.filter_by(youtube_video_id=None).all()
# for location in locations:
# 	video_id = get_location_video(location.city_name, location.state)
# 	print(location, video_id)
# 	location.youtube_video_id = video_id
# db.session.commit()

# Update one location's video using the city naem
# location = Location.query.filter_by(city_name='Belle Glade').one()
# video_id = 'jCLp6eiT4z8'
# print(location, video_id)
# location.youtube_video_id = video_id
# db.session.commit()
