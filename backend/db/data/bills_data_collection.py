import json
import requests

URL = "https://api.propublica.org/congress/v1/bills/subjects/"
headers = {"X-API-KEY": "vV6ltXMwQnuVXBsSPP31Y6DGJPv5oMsbUafpXDL5"}

subjects = [
    "air-quality",
    "air-pollution",
    "air-pollution-control",
    "climate-change-and-greenhouse-gases",
]

# API only gets the 20 most recent bills for each subject, so combining multiple to get enough instances for our models
bills_data = {}
for subject in subjects:
    request_url = URL + subject + ".json"
    r = requests.get(url=request_url, headers=headers)
    bills_data[subject] = r.json()

model_id = 1
for subject in bills_data:
    for bill in bills_data[subject]["results"]:
        bill["model_id"] = model_id

        sponsor = bill["sponsor_name"]
        sponsor = sponsor.replace(" ", "%20")
        url = (
            'https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch="%s"%%20politician&utf8=&format=json&srlimit=1'
            % (sponsor)
        )
        search_response = json.loads(urlopen(url).read())
        wiki_title = search_response["query"]["search"][0]["title"]

        wiki_title = wiki_title.replace(" ", "%20")
        url = (
            "https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles=%s"
            % (wiki_title)
        )
        image_response = json.loads(urlopen(url).read())
        for page in image_response["query"]["pages"]:
            try:
                image_source = image_response["query"]["pages"][page]["original"][
                    "source"
                ]
                bill["sponsor_image"] = image_source
                print(image_source)
            except KeyError:
                bill["sponsor_image"] = None
        model_id += 1

with open("bills_data.json", "w") as outfile:
    json.dump(bills_data, outfile)
