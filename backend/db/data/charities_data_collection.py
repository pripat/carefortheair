import json
import requests
from html.parser import HTMLParser


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return "".join(self.fed)


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


URL = "https://api.data.charitynavigator.org/v2/Organizations?app_id=11f5c6a3&app_key=560f8533c9a7fcf733945bc18706e4de&search=air&categoryID=4&causeID=11"

r = requests.get(url=URL)

data = r.json()
model_id = 1
for charity in data:
    charity["model_id"] = model_id
    model_id += 1
    charity["mission"] = strip_tags(charity["mission"])

with open("charities_data.json", "w") as outfile:
    json.dump(data, outfile)
