import json
import requests
from urllib.request import urlopen
from urllib.parse import urlencode
from html.parser import HTMLParser
import us


def getplace(lat, lon):
    key = ""  # Put Google API key here
    url = "https://maps.googleapis.com/maps/api/geocode/json?"
    url += "latlng=%s,%s&sensor=false&key=%s" % (lat, lon, key)
    v = urlopen(url).read()
    j = json.loads(v)
    try:
        return j["plus_code"]["compound_code"]
    except KeyError:
        return None


def parse_address_code(address_code):
    tokens = address_code.split(",")
    state = tokens[-2]
    state = state[1:]
    city = tokens[-3]
    city_tokens = list(token for token in city.split() if "+" not in token)
    city = " ".join(city_tokens)
    return (city, state)


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.strict = False
        self.convert_charrefs = True
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return "".join(self.fed)


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()


def get_first_paragraph(text):
    try:
        first_index = len(text) - len(text.lstrip())
        end_of_paragraph = text[first_index:].index("\n") + first_index
        return text[first_index:end_of_paragraph]
    except ValueError:
        return text


def parse_address_code(address_code):
    tokens = address_code.split(",")
    try:
        state = tokens[-2]
        state = state[1:]
    except IndexError:
        state = None
    try:
        city = tokens[-3]
        city_tokens = list(token for token in city.split() if "+" not in token)
        city = " ".join(city_tokens)
    except:
        city = None
    return (city, state)


# https://en.wikipedia.org/wiki/List_of_extreme_points_of_the_United_States
latlng_data_URL = "https://api.waqi.info/map/bounds/?token=373d396ce7b5c7a45269ede2d7a646e0e64687f7&latlng=18,-180,72,-66"

r = requests.get(url=latlng_data_URL, params=None)

data = r.json()

latlng_list = data["data"]

us_locations = [
    "/usa/",
    "/texas/",
    "/california/",
    "/utah/",
    "/georgia/",
    "/losangeles/",
]

location_data = []
for latlng in latlng_list:
    location_data_URL = (
        "https://api.waqi.info/feed/geo:"
        + str(latlng["lat"])
        + ";"
        + str(latlng["lon"])
        + "/?token=373d396ce7b5c7a45269ede2d7a646e0e64687f7"
    )
    r = requests.get(url=location_data_URL, params=None)
    data = r.json()
    city_url = data["data"]["city"]["url"]
    if any(x in city_url for x in us_locations):
        address_code = getplace(latlng["lat"], latlng["lon"])
        if address_code is not None:
            data["data"]["city"]["address_code"] = address_code
            location_data.append(data)

    for location in location_data:
        city, state = parse_address_code(location["data"]["city"]["address_code"])
        try:
            state = us.states.lookup(state).name
        except:
            address_name = location["data"]["city"]["name"]
            tokens = address_name.split(",")
            city = tokens[-3]
            state = tokens[-2]
        location["data"]["city"]["name"] = city
        location["data"]["city"]["state"] = state

        service_url = "https://public.opendatasoft.com/api/records/1.0/search/"
        params = {
            "dataset": "worldcitiespop",
            "q": city,
            "lang": "en",
            "sort": "population",
            "refine.country": "us",
            "refine.region": us_state_abbrev[state.lstrip()],
        }
        url = service_url + "?" + urlencode(params)
        response = json.loads(urlopen(url).read())
        for record in response["records"]:
            fields = record["fields"]
            if city.lower() == fields["city"].lower():
                location["data"]["city"]["population"] = fields.get("population")

        city = city.replace(" ", "%20")
        state = state.replace(" ", "%20")
        url = (
            "https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=%s,%s&utf8=&format=json&srlimit=1"
            % (city, state)
        )
        search_response = json.loads(urlopen(url).read())
        wiki_title = search_response["query"]["search"][0]["title"]

        wiki_data = {}
        wiki_title = wiki_title.replace(" ", "%20")
        url = (
            "https://en.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&exintro=&titles=%s"
            % (wiki_title)
        )
        extract_response = json.loads(urlopen(url).read())
        for page in extract_response["query"]["pages"]:
            text = extract_response["query"]["pages"][page]["extract"]
            text = strip_tags(text)
            text = get_first_paragraph(text)
            wiki_data["text"] = text

        url = (
            "https://en.wikipedia.org/w/api.php?action=query&prop=pageimages&format=json&piprop=original&titles=%s"
            % (wiki_title)
        )
        image_response = json.loads(urlopen(url).read())
        for page in image_response["query"]["pages"]:
            try:
                image_source = image_response["query"]["pages"][page]["original"][
                    "source"
                ]
                wiki_data["image_source"] = image_source
            except KeyError:
                image_source = None

        location["data"]["city"]["wiki_data"] = wiki_data
        print(location)

with open("location_data.json", "w") as outfile:
    json.dump(location_data, outfile)
