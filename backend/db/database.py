import flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine, Column, Integer, String
from sqlalchemy.orm import sessionmaker, configure_mappers
from models import Location, Bill, Charity, db
import json
import us
from datetime import date
import random
from data.bills_photos import bills_photos
from data.charities_photos import stock_photos, charities_photos
from data.charities_social import charities_facebook
from data.bills_social import bill_sponsors_twitter
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError


def get_location_video(city, state):
    try:
        youtube = build(
            "youtube", "v3", developerKey="AIzaSyDX4M2XCF8OInSZY-jG1ev49JAaJDJ3C28"
        )

        query_string = "{} {} tour".format(city, state)

        # Call the search.list method to retrieve results matching the specified
        # query term.
        search_response = (
            youtube.search()
            .list(
                q=query_string,
                type="video",
                part="id,snippet",
                maxResults=1,
                safeSearch="strict",
                regionCode="us",
                videoEmbeddable="true",
                videoCategoryId="19",  # category id for travel & events
                order="viewCount",
            )
            .execute()
        )

        return search_response["items"][0]["id"]["videoId"]
    except HttpError:
        return None
    except IndexError:
        return None


def get_related_location_id(state):
    id_list = []
    all_ids = []
    with open("data/location_data.json") as json_file:
        location_data = json.load(json_file)
        for location in location_data:
            model_id = location["data"]["idx"]
            if location["data"]["city"]["state"] == state:
                id_list.append(model_id)
            all_ids.append(model_id)
    if len(id_list) > 0:
        return random.choice(id_list)
    else:
        return random.choice(all_ids)


def get_location_related_bill_id(state):
    id_list = []
    all_ids = []
    with open("data/bills_data.json") as json_file:
        bills_data = json.load(json_file)
        for subject in bills_data:
            for bill in bills_data[subject]["results"]:
                if us.states.lookup(bill["sponsor_state"]).name == state:
                    id_list.append(bill["model_id"])
                all_ids.append(bill["model_id"])
    if len(id_list) > 0:
        return random.choice(id_list)
    else:
        return random.choice(all_ids)


def get_location_related_charity_id(state):
    id_list = []
    all_ids = []
    with open("data/charities_data.json") as json_file:
        charities_data = json.load(json_file)
        for charity in charities_data:
            model_id = charity["model_id"]
            if (
                us.states.lookup(charity["mailingAddress"]["stateOrProvince"]).name
                == state
            ):
                id_list.append(model_id)
            all_ids.append(model_id)
    if len(id_list) > 0:
        return random.choice(id_list)
    else:
        return random.choice(all_ids)


def get_bill_related_charity_id(subject):
    bills_to_charities = {
        "health": [
            "Consumer Protection, Safety",
            "Water Resource, Wetlands Conservation and Management",
            "Pollution Abatement and Control Services",
        ],
        "environmental protection": [
            "Forest Conservation",
            "Water Resource, Wetlands Conservation and Management",
            "Pollution Abatement and Control Services",
            "Natural Resources Conservation and Protection",
            "Wildlife Preservation, Protection",
            "Zoo, Zoological Society",
        ],
        "transportation and public works": [
            "Recreational and Sporting Camps",
            "Public Interest Law, Litigation",
            "Research Institutes and/or Public Policy Analysis ",
        ],
        "housing and community development": [
            "Land Resources Conservation",
            "Consumer Protection, Safety",
        ],
        "science, technology, communications": [
            "Research Institutes and/or Public Policy Analysis ",
            "Education N.E.C.",
        ],
        "government operations and politics": [
            "Public Interest Law, Litigation",
            "Research Institutes and/or Public Policy Analysis ",
            "Alliance/Advocacy Organizations ",
        ],
        "public lands and natural resources": [
            "Public Interest Law, Litigation",
            "Water Resource, Wetlands Conservation and Management",
            "Land Resources Conservation",
            "Natural Resources Conservation and Protection",
        ],
        "economics and public finance": ["Fund Raising and/or Fund Distribution "],
        "congress": [
            "Research Institutes and/or Public Policy Analysis ",
            "Public Interest Law, Litigation",
            "Alliance/Advocacy Organizations ",
        ],
        "taxation": ["Fund Raising and/or Fund Distribution ", "Other"],
        "energy": [
            "Pollution Abatement and Control Services",
            "Natural Resources Conservation and Protection",
            "Water Resource, Wetlands Conservation and Management",
        ],
        "commerce": ["Fund Raising and/or Fund Distribution ", "Other"],
        "international affairs": ["International Relief"],
    }
    classifications = bills_to_charities.get(subject.lower())
    id_list = []
    with open("data/charities_data.json") as json_file:
        charities_data = json.load(json_file)
        for charity in charities_data:
            classification = charity["irsClassification"]["nteeClassification"]
            if classification is None:
                classification = "Other"
            if classification in classifications:
                id_list.append(charity["model_id"])
    return random.choice(id_list)


def get_charity_related_bill_id(classification):
    charities_to_bills = {
        "Wildlife Preservation, Protection": [
            "public lands and natural resources",
            "environmental protection",
        ],
        "Alliance/Advocacy Organizations ": [
            "congress",
            "government operations and politics",
        ],
        "Forest Conservation": [
            "public lands and natural resources",
            "environmental protection",
        ],
        "Recreational and Sporting Camps": [
            "transportation and public works",
            "housing and community development",
        ],
        "International Relief": ["international affairs"],
        "Fund Raising and/or Fund Distribution ": [
            "commerce",
            "taxation",
            "economics and public finance",
        ],
        "Research Institutes and/or Public Policy Analysis ": [
            "government operations and politics",
            "science, technology, communications",
        ],
        "Zoo, Zoological Society": ["environmental protection"],
        "Education N.E.C.": [
            "housing and community development",
            "science, technology, communications",
        ],
        "Natural Resources Conservation and Protection": [
            "energy",
            "public lands and natural resources",
            "environmental protection",
        ],
        "Land Resources Conservation": [
            "public lands and natural resources",
            "environmental protection",
        ],
        "Consumer Protection, Safety": ["health"],
        "Pollution Abatement and Control Services": [
            "energy",
            "environmental protection",
            "health",
        ],
        "Water Resource, Wetlands Conservation and Management": [
            "energy",
            "public lands and natural resources",
            "environmental protection",
            "health",
        ],
        "Public Interest Law, Litigation": [
            "public lands and natural resources",
            "government operations and politics",
        ],
        "Other": [
            "commerce",
            "energy",
            "taxation",
            "congress",
            "economics and public finance",
            "public lands and natural resources",
            "government operations and politics",
            "health",
            "environmental protection",
            "transportation and public works",
            "science, technology, communications",
        ],
    }
    subjects = charities_to_bills.get(classification)
    id_list = []
    with open("data/bills_data.json") as json_file:
        bills_data = json.load(json_file)
        for subject in bills_data:
            for bill in bills_data[subject]["results"]:
                if bill["primary_subject"].lower() in subjects:
                    id_list.append(bill["model_id"])
    return random.choice(id_list)


def get_date_from_string(s):
    tokens = s.split("-")
    year = int(tokens[0])
    month = int(tokens[1])
    day = int(tokens[2])
    return date(year, month, day)


def get_air_status(aqi):
    assert aqi >= 0
    if aqi < 51:
        return "Good"
    elif aqi < 101:
        return "Moderate"
    elif aqi < 151:
        return "Unhealthy for sensitive groups"
    elif aqi < 201:
        return "Unhealthy"
    elif aqi < 301:
        return "Very unhealthy"
    else:
        return "Hazardous"


def get_bill_type_string(type):
    types = {
        "s": "Senate bill",
        "hjres": "House joint resolution",
        "sjres": "Senate joint resolution",
        "sres": "Senate resolution",
        "hres": "House resolution",
        "hr": "House bill",
        "hconres": "House concurrent resolution",
    }
    return types.get(type, None)


def get_party_string(party):
    parties = {"R": "Republican", "D": "Democratic"}
    return parties.get(party, None)


def get_enacted_string(enacted):
    if enacted:
        return "Yes"
    else:
        return "No"


def populate_locations_tables():
    with open("data/location_data.json") as json_file:
        location_data = json.load(json_file)
        for location in location_data:
            population = location["data"]["city"].get("population", None)
            population_string = None
            if population is not None:
                population_string = "{:,}".format(population)
            location_model = Location(
                model_id=location["data"]["idx"],
                aqi=location["data"]["aqi"],
                aqi_string=str(location["data"]["aqi"]),
                air_status=get_air_status(location["data"]["aqi"]),
                city_name=location["data"]["city"]["name"],
                population=population,
                population_string=population_string,
                latitude=location["data"]["city"]["geo"][0],
                longitude=location["data"]["city"]["geo"][1],
                aqicn_url=location["data"]["city"]["url"],
                state=location["data"]["city"]["state"],
                wiki_text=location["data"]["city"]["wiki_data"]["text"],
                wiki_image=location["data"]["city"]["wiki_data"].get(
                    "image_source", None
                ),
                dominant_pol=location["data"]["dominentpol"],
                pollutant_values=location["data"]["iaqi"],
                related_bill_id=get_location_related_bill_id(
                    location["data"]["city"]["state"]
                ),
                related_charity_id=get_location_related_charity_id(
                    location["data"]["city"]["state"]
                ),
                youtube_video_id=get_location_video(
                    location["data"]["city"]["name"], location["data"]["city"]["state"]
                ),
            )
            db.session.merge(location_model)


def populate_bills_tables():
    with open("data/bills_data.json") as json_file:
        bills_data = json.load(json_file)
        for subject in bills_data:
            for bill in bills_data[subject]["results"]:
                if bill["short_title"] is None:
                    bill["short_title"] = bill["number"]
                bill_model = Bill(
                    model_id=bill["model_id"],
                    bill_id=bill["bill_id"],
                    bill_slug=bill["bill_slug"],
                    bill_type=get_bill_type_string(bill["bill_type"]),
                    bill_type_abbr=bill["bill_type"],
                    bill_number=bill["number"],
                    bill_image=bills_photos.get(bill["primary_subject"]),
                    primary_subject=bill["primary_subject"],
                    title=bill["title"],
                    short_title=bill["short_title"],
                    sponsor_name=bill["sponsor_name"],
                    sponsor_title=bill["sponsor_title"],
                    sponsor_state=us.states.lookup(bill["sponsor_state"]).name,
                    sponsor_party=get_party_string(bill["sponsor_party"]),
                    introduction_date=get_date_from_string(bill["introduced_date"]),
                    latest_action_date=get_date_from_string(
                        bill["latest_major_action_date"]
                    ),
                    latest_action=bill["latest_major_action"],
                    enacted=bool(bill["enacted"]),
                    enacted_string=get_enacted_string(bool(bill["enacted"])),
                    summary=bill["summary"],
                    short_summary=bill["summary_short"],
                    sponsor_image=bill["sponsor_image"],
                    committees=bill["committees"],
                    bill_pdf=bill["gpo_pdf_uri"],
                    related_location_id=get_related_location_id(
                        us.states.lookup(bill["sponsor_state"]).name
                    ),
                    related_charity_id=get_bill_related_charity_id(
                        bill["primary_subject"]
                    ),
                    sponsor_twitter=bill_sponsors_twitter.get(bill["sponsor_name"]),
                )
                db.session.merge(bill_model)


def populate_charities_tables():
    with open("data/charities_data.json") as json_file:
        charities_data = json.load(json_file)
        for charity in charities_data:
            photo = charities_photos[charity["charityName"]]
            if photo is None:
                photo = random.choice(stock_photos)
            ntee_classification = charity["irsClassification"]["nteeClassification"]
            if ntee_classification is None:
                ntee_classification = "Other"
            charity_model = Charity(
                model_id=charity["model_id"],
                name=charity["charityName"],
                mission=charity["mission"],
                tag_line=charity["tagLine"],
                rating=charity["currentRating"]["rating"],
                rating_image=charity["currentRating"]["ratingImage"]["large"],
                category=charity["category"]["categoryName"],
                cause=charity["cause"]["causeName"],
                foundation_status=charity["irsClassification"]["foundationStatus"],
                asset_amount=charity["irsClassification"]["assetAmount"],
                income_amount=charity["irsClassification"]["incomeAmount"],
                website_url=charity["websiteURL"],
                charity_photo=photo,
                city=charity["mailingAddress"]["city"],
                state=us.states.lookup(
                    charity["mailingAddress"]["stateOrProvince"]
                ).name,
                related_location_id=get_related_location_id(
                    us.states.lookup(charity["mailingAddress"]["stateOrProvince"]).name
                ),
                related_bill_id=get_charity_related_bill_id(ntee_classification),
                facebook_username=charities_facebook.get(charity["charityName"]),
                ntee_classification=ntee_classification,
            )
            db.session.merge(charity_model)


if __name__ == "__main__":
    db.drop_all()
    db.configure_mappers()
    db.create_all()
    db.session.commit()
    populate_locations_tables()
    populate_bills_tables()
    populate_charities_tables()
    db.session.commit()
