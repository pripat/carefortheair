import flask
from flask_sqlalchemy import SQLAlchemy, BaseQuery
from flask_restless import APIManager
from sqlalchemy.dialects.postgresql.json import JSONB
from sqlalchemy_searchable import SearchQueryMixin
from sqlalchemy_utils.types import TSVectorType
from sqlalchemy_searchable import make_searchable

app = flask.Flask(__name__)
app.config["DEBUG"] = True
app.config[
    "SQLALCHEMY_DATABASE_URI"
] = "postgresql://postgres:postgres@postgres.cp6xwexjc6ms.us-east-1.rds.amazonaws.com/postgres"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)

make_searchable(db.metadata)


class LocationQuery(BaseQuery, SearchQueryMixin):
    pass


class Location(db.Model):
    query_class = LocationQuery
    __tablename__ = "locations"

    model_id = db.Column(db.Integer, primary_key=True)
    aqi = db.Column(db.Integer)
    aqi_string = db.Column(db.String)
    air_status = db.Column(db.String)
    city_name = db.Column(db.String)
    population = db.Column(db.Integer)
    population_string = db.Column(db.String)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    aqicn_url = db.Column(db.String)
    state = db.Column(db.String)
    wiki_text = db.Column(db.Text)
    wiki_image = db.Column(db.String)
    dominant_pol = db.Column(db.String)
    pollutant_values = db.Column(JSONB)
    related_bill_id = db.Column(db.Integer)
    related_charity_id = db.Column(db.Integer)
    youtube_video_id = db.Column(db.String)

    search_attributes = [
        "aqi_string",
        "air_status",
        "city_name",
        "population_string",
        "state",
        "wiki_text",
        "dominant_pol",
    ]
    filter_attributes = {
        "state": "state",
        "dominant-pol": "dominant_pol",
        "air-status": "air_status",
    }
    sort_attributes = {"name": "city_name", "aqi": "aqi", "population": "population"}

    def jsonify(self):
        return {
            "model_id": self.model_id,
            "aqi": self.aqi,
            "aqi_string": self.aqi_string,
            "air_status": self.air_status,
            "city_name": self.city_name,
            "population": self.population,
            "population_string": self.population_string,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "aqicn_url": self.aqicn_url,
            "state": self.state,
            "wiki_text": self.wiki_text,
            "wiki_image": self.wiki_image,
            "dominant_pol": self.dominant_pol,
            "pollutant_values": self.pollutant_values,
            "related_bill_id": self.related_bill_id,
            "related_charity_id": self.related_charity_id,
            "youtube_video_id": self.youtube_video_id,
        }

    def get_attribute_label(attribute):
        location_labels = {
            "aqi_string": "Air Quality Index",
            "air_status": "Air Status",
            "city_name": "City",
            "population_string": "Population",
            "state": "State",
            "wiki_text": "Description",
            "dominant_pol": "Dominant Pollutant",
        }
        return location_labels.get(attribute)

    def __eq__(self, other):
        if not isinstance(other, Location):
            return False
        return self.model_id == other.model_id


class BillQuery(BaseQuery, SearchQueryMixin):
    pass


class Bill(db.Model):
    query_class = BillQuery
    __tablename__ = "bills"

    model_id = db.Column(db.Integer, primary_key=True)
    bill_id = db.Column(db.String)
    bill_slug = db.Column(db.String)
    bill_type = db.Column(db.String)
    bill_type_abbr = db.Column(db.String)
    bill_number = db.Column(db.String)
    bill_image = db.Column(db.String)
    primary_subject = db.Column(db.String)
    title = db.Column(db.String)
    short_title = db.Column(db.String)
    sponsor_name = db.Column(db.String)
    sponsor_title = db.Column(db.String)
    sponsor_state = db.Column(db.String)
    sponsor_party = db.Column(db.String)
    introduction_date = db.Column(db.Date)
    latest_action_date = db.Column(db.Date)
    latest_action = db.Column(db.String)
    enacted = db.Column(db.Boolean)
    enacted_string = db.Column(db.String)
    summary = db.Column(db.Text)
    short_summary = db.Column(db.Text)
    sponsor_image = db.Column(db.String)
    committees = db.Column(db.String)
    bill_pdf = db.Column(db.String)
    related_location_id = db.Column(db.Integer)
    related_charity_id = db.Column(db.Integer)
    sponsor_twitter = db.Column(db.String)

    search_attributes = [
        "bill_type",
        "short_title",
        "primary_subject",
        "title",
        "sponsor_name",
        "sponsor_title",
        "enacted_string",
        "committees",
        "latest_action",
        "sponsor_state",
        "sponsor_party",
        "sponsor_twitter",
    ]
    filter_attributes = {
        "bill-type": "bill_type",
        "party": "sponsor_party",
        "subject": "primary_subject",
    }
    sort_attributes = {"title": "short_title", "action-date": "latest_action_date"}

    def jsonify(self):
        return {
            "model_id": self.model_id,
            "bill_id": self.bill_id,
            "bill_slug": self.bill_slug,
            "bill_type": self.bill_type,
            "bill_type_abbr": self.bill_type_abbr,
            "bill_number": self.bill_number,
            "bill_image": self.bill_image,
            "primary_subject": self.primary_subject,
            "title": self.title,
            "short_title": self.short_title,
            "sponsor_name": self.sponsor_name,
            "sponsor_title": self.sponsor_title,
            "sponsor_state": self.sponsor_state,
            "sponsor_party": self.sponsor_party,
            "introduction_date": self.introduction_date,
            "latest_action_date": self.latest_action_date,
            "latest_action": self.latest_action,
            "enacted": self.enacted,
            "enacted_string": self.enacted_string,
            "summary": self.summary,
            "short_summary": self.short_summary,
            "sponsor_image": self.sponsor_image,
            "committees": self.committees,
            "bill_pdf": self.bill_pdf,
            "related_location_id": self.related_location_id,
            "related_charity_id": self.related_charity_id,
            "sponsor_twitter": self.sponsor_twitter,
        }

    def get_attribute_label(attribute):
        bill_labels = {
            "bill_type": "Bill Type",
            "short_title": "Short Title",
            "title": "Title",
            "primary_subject": "Primary Subject",
            "sponsor_name": "Sponsor Name",
            "sponsor_title": "Sponsor Title",
            "enacted_string": "Enacted",
            "committees": "Committees",
            "latest_action": "Latest Major Action",
            "sponsor_state": "Sponsor's State",
            "sponsor_party": "Sponsoring Party",
            "sponsor_twitter": "Sponsor Twitter",
        }
        return bill_labels.get(attribute)


class CharityQuery(BaseQuery, SearchQueryMixin):
    pass


class Charity(db.Model):
    query_class = CharityQuery
    __tablename__ = "charities"

    model_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    mission = db.Column(db.Text)
    tag_line = db.Column(db.Text)
    rating = db.Column(db.Integer)
    rating_image = db.Column(db.String)
    category = db.Column(db.String)
    cause = db.Column(db.String)
    foundation_status = db.Column(db.Text)
    asset_amount = db.Column(db.Integer)
    income_amount = db.Column(db.Integer)
    website_url = db.Column(db.String)
    charity_photo = db.Column(db.String)
    city = db.Column(db.String)
    state = db.Column(db.String)
    related_location_id = db.Column(db.Integer)
    related_bill_id = db.Column(db.Integer)
    facebook_username = db.Column(db.String)
    ntee_classification = db.Column(db.String)

    search_attributes = [
        "name",
        "mission",
        "tag_line",
        "category",
        "cause",
        "foundation_status",
        "city",
        "state",
        "facebook_username",
        "ntee_classification",
    ]
    filter_attributes = {
        "state": "state",
        "classification": "ntee_classification",
        "rating": "rating",
    }
    sort_attributes = {"name": "name", "rating": "rating"}

    def jsonify(self):
        return {
            "model_id": self.model_id,
            "name": self.name,
            "mission": self.mission,
            "tag_line": self.tag_line,
            "rating": self.rating,
            "rating_image": self.rating_image,
            "category": self.category,
            "cause": self.cause,
            "foundation_status": self.foundation_status,
            "asset_amount": self.asset_amount,
            "income_amount": self.income_amount,
            "website_url": self.website_url,
            "charity_photo": self.charity_photo,
            "city": self.city,
            "state": self.state,
            "related_location_id": self.related_location_id,
            "related_bill_id": self.related_bill_id,
            "facebook_username": self.facebook_username,
            "ntee_classification": self.ntee_classification,
        }

    def get_attribute_label(attribute):
        charity_labels = {
            "name": "Name",
            "mission": "Mission",
            "tag_line": "Tagline",
            "category": "Category",
            "cause": "Cause",
            "foundation_status": "Foundation Status",
            "city": "City",
            "state": "State",
            "facebook_username": "Facebook Username",
            "ntee_classification": "Classification",
        }
        return charity_labels.get(attribute)
