import unittest
import main
from db.models import Location, Bill, Charity, app, db
import json
from datetime import date


class Test(unittest.TestCase):
    def test_all_locations(self):
        with app.app_context():
            self.assertEqual(len(main.get_all_locations(None).json), 380)

    def test_one_location(self):
        with app.app_context():
            response = main.get_all_locations(262).json
            self.assertEqual(len(response), 1)
            self.assertEqual(response[0]["city_name"], "Santa Barbara")
            self.assertEqual(response[0]["model_id"], 262)

    def test_location_search(self):
        with app.test_client() as c:
            r = c.get("/locations/search", query_string={"query": "bar"})
            search_results, query = main.search(
                db_query=Location.query, db_model=Location
            )
            self.assertEqual(query, "bar")
            for location in search_results:
                location_json = location.jsonify()
                if location_json["population_string"] is None:
                    location_json["population_string"] = ""
                found_query = False
                for attribute in Location.search_attributes:
                    found_query = found_query or (
                        "bar" in location_json[attribute].lower()
                    )
                self.assertTrue(found_query)

    def test_location_filter(self):
        with app.test_client() as c:
            r = c.get(
                "/locations/search",
                query_string={
                    "state": "virginia",
                    "air-status": "good",
                    "dominant_pol": "o3",
                },
            )
            filter_results = main.filter(db_query=Location.query, db_model=Location)
            for location in filter_results:
                location_json = location.jsonify()
                self.assertEqual(location_json["state"], "Virginia")
                self.assertEqual(location_json["air_status"], "Good")
                self.assertEqual(location_json["dominant_pol"], "o3")

    def test_location_sort(self):
        with app.test_client() as c:
            r = c.get("/locations/search", query_string={"sort": "population"})
            sort_results = main.sort(db_query=Location.query, db_model=Location)
            prev_population = 0
            for location in sort_results:
                location_json = location.jsonify()
                population = location_json["population"]
                if population is not None:
                    self.assertTrue(location_json["population"] >= prev_population)
                    prev_population = location_json["population"]

    def test_location_sort_search_filter_1(self):
        with app.test_client() as c:
            r = c.get(
                "/locations/search",
                query_string={
                    "query": "santa",
                    "state": "california",
                    "air-status": "good",
                    "sort": "aqi",
                    "desc": "true",
                },
            )
            results = main.search_filter_sort_locations().json.get("results")
            prev_aqi = 500
            for location in results:
                self.assertTrue(prev_aqi > location["aqi"])
                found_query = False
                for attribute in Location.search_attributes:
                    try:
                        found_query = found_query or (
                            "santa" in location[attribute].lower()
                        )
                    except AttributeError:
                        pass
                self.assertTrue(found_query)
                self.assertEqual(location["air_status"], "Good")
                self.assertEqual(location["state"], "California")

    def test_location_sort_search_filter_2(self):
        with app.test_client() as c:
            r = c.get("locations/search", query_string={})
            results = main.search_filter_sort_locations().json.get("results")
            self.assertEqual(results, main.get_all_locations(None).json)

    def test_location_related_bill(self):
        with app.app_context():
            data = main.get_locations_by_related_bill(1).json
            for location in data:
                self.assertEqual(location["related_bill_id"], 1)

    def test_location_related_charity(self):
        with app.app_context():
            data = main.get_locations_by_related_charity(1).json
            for location in data:
                self.assertEqual(location["related_charity_id"], 1)

    def test_all_bills(self):
        with app.app_context():
            self.assertEqual(len(main.get_all_bills(None).json), 55)

    def test_one_bill(self):
        with app.app_context():
            response = main.get_all_bills(1).json
            self.assertEqual(len(response), 1)
            self.assertEqual(response[0]["bill_id"], "sjres8-116")
            self.assertEqual(response[0]["model_id"], 1)

    def test_bill_search(self):
        with app.test_client() as c:
            r = c.get("/bills/search", query_string={"query": "energy"})
            search_results, query = main.search(db_query=Bill.query, db_model=Bill)
            self.assertEqual(query, "energy")
            for bill in search_results:
                bill_json = bill.jsonify()
                found_query = False
                for attribute in Bill.search_attributes:
                    found_query = found_query or (
                        "energy" in bill_json[attribute].lower()
                    )
                self.assertTrue(found_query)

    def test_bill_filter(self):
        with app.test_client() as c:
            r = c.get(
                "/bills/search",
                query_string={
                    "bill-type": "senate bill",
                    "party": "dem",
                    "subject": "environmental protection",
                },
            )
            filter_results = main.filter(db_query=Bill.query, db_model=Bill)
            for bill in filter_results:
                bill_json = bill.jsonify()
                self.assertEqual(bill_json["bill_type"], "Senate bill")
                self.assertEqual(bill_json["sponsor_party"], "Democratic")
                self.assertEqual(
                    bill_json["ntee_classification"], "Environmental Protection"
                )

    def test_bill_sort(self):
        with app.test_client() as c:
            r = c.get(
                "/bills/search", query_string={"sort": "action-date", "desc": True}
            )
            sort_results = main.sort(db_query=Bill.query, db_model=Bill)
            prev_date = date.today()
            for bill in sort_results:
                bill_json = bill.jsonify()
                self.assertTrue(bill_json["latest_action_date"] <= prev_date)
                prev_date = bill_json["latest_action_date"]

    def test_bill_sort_search_filter_1(self):
        with app.test_client() as c:
            r = c.get(
                "/bills/search",
                query_string={
                    "query": "energy",
                    "subject": "taxation",
                    "party": "dem",
                    "sort": "title",
                    "desc": "false",
                },
            )
            results = main.search_filter_sort_bills().json.get("results")
            prev_title = ""
            for bill in results:
                self.assertTrue(prev_title < bill["short_title"])
                self.assertTrue("energy" in bill["short_title"].lower())
                self.assertEqual(bill["primary_subject"], "Taxation")
                self.assertEqual(bill["sponsor_party"], "Democratic")

    def test_bill_sort_search_filter_2(self):
        with app.test_client() as c:
            r = c.get("bills/search", query_string={})
            results = main.search_filter_sort_bills().json.get("results")
            self.assertEqual(results, main.get_all_bills(None).json)

    def test_bill_related_location(self):
        with app.app_context():
            data = main.get_bills_by_related_location(7329).json
            for bill in data:
                self.assertEqual(bill["related_location_id"], 7329)

    def test_bill_related_charity(self):
        with app.app_context():
            data = main.get_bills_by_related_charity(10).json
            for bill in data:
                self.assertEqual(bill["related_charity_id"], 10)

    def test_all_charities(self):
        with app.app_context():
            self.assertEqual(len(main.get_all_charities(None).json), 45)

    def test_one_charity(self):
        with app.app_context():
            response = main.get_all_charities(1).json
            self.assertEqual(len(response), 1)
            self.assertEqual(response[0]["name"], "Coalition for Clean Air")
            self.assertEqual(response[0]["model_id"], 1)

    def test_charity_search(self):
        with app.test_client() as c:
            r = c.get("/charities/search", query_string={"query": "wash"})
            search_results, query = main.search(
                db_query=Charity.query, db_model=Charity
            )
            self.assertEqual(query, "wash")
            for charity in search_results:
                charity_json = charity.jsonify()
                found_query = False
                for attribute in Charity.search_attributes:
                    found_query = found_query or (
                        "wash" in charity_json[attribute].lower()
                    )
                self.assertTrue(found_query)

    def test_charity_filter(self):
        with app.test_client() as c:
            r = c.get(
                "/charities/search", query_string={"state": "california", "rating": 3}
            )
            filter_results = main.filter(db_query=Charity.query, db_model=Charity)
            for charity in filter_results:
                charity_json = charity.jsonify()
                self.assertEqual(charity_json["state"], "California")
                self.assertEqual(charity_json["rating"], 3)

    def test_charity_sort(self):
        with app.test_client() as c:
            r = c.get("/charities/search", query_string={"sort": "name"})
            sort_results = main.sort(db_query=Charity.query, db_model=Charity)
            prev_name = ""
            for charity in sort_results.limit(10):
                charity_json = charity.jsonify()
                self.assertTrue(prev_name.lower() < charity_json["name"].lower())
                prev_name = charity_json["name"]

    def test_charity_sort_search_filter_1(self):
        with app.test_client() as c:
            r = c.get(
                "/charities/search",
                query_string={
                    "query": "water",
                    "category": "environment",
                    "rating": 4,
                    "sort": "name",
                    "desc": "false",
                },
            )
            results = main.search_filter_sort_charities().json.get("results")
            prev_name = ""
            for charity in results:
                self.assertTrue(prev_name < charity["name"])
                self.assertEqual(charity["category"], "Environment")
                self.assertEqual(charity["rating"], 4)

    def test_charity_sort_search_filter_2(self):
        with app.test_client() as c:
            r = c.get("charities/search", query_string={})
            results = main.search_filter_sort_charities().json.get("results")
            self.assertEqual(results, main.get_all_charities(None).json)

    def test_charity_related_location(self):
        with app.app_context():
            data = main.get_charities_by_related_location(249).json
            for charity in data:
                self.assertEqual(charity["related_location_id"], 249)

    def test_charity_related_bill(self):
        with app.app_context():
            data = main.get_charities_by_related_bill(62).json
            for charity in data:
                self.assertEqual(charity["related_bill_id"], 62)

    def test_get_all_models(self):
        with app.app_context():
            data = main.get_all_models().json
            self.assertEqual(data["locations"], main.get_all_locations(None).json)
            self.assertEqual(data["bills"], main.get_all_bills(None).json)
            self.assertEqual(data["charities"], main.get_all_charities(None).json)

    def test_search_all_models(self):
        with app.test_client() as c:
            r = c.get("/charities/search", query_string={"query": "water"})
            response = main.search_all_models().json
            self.assertEqual(
                response["locations"], main.search_filter_sort_locations().json
            )
            self.assertEqual(response["bills"], main.search_filter_sort_bills().json)
            self.assertEqual(
                response["charities"], main.search_filter_sort_charities().json
            )

    def test_db_location_insert(self):
        test_location = Location(model_id=-1)

        try:
            db.session.add(test_location)
            db.session.commit()
            response = Location.query.filter_by(model_id=-1).one()
            self.assertEqual(response.model_id, -1)
            db.session.delete(response)
            db.session.commit()
        except Exception as e:
            print(e)
            db.sessions.rollback()
            self.assertTrue(False)

    def test_db_bill_insert(self):
        test_bill = Bill(model_id=-1)

        try:
            db.session.add(test_bill)
            db.session.commit()
            response = Bill.query.filter_by(model_id=-1).one()
            self.assertEqual(response.model_id, -1)
            db.session.delete(response)
            db.session.commit()
        except Exception as e:
            print(e)
            db.sessions.rollback()
            self.assertTrue(False)

    def test_db_charity_insert(self):
        test_charity = Charity(model_id=-1)

        try:
            db.session.add(test_charity)
            db.session.commit()
            response = Charity.query.filter_by(model_id=-1).one()
            self.assertEqual(response.model_id, -1)
            db.session.delete(response)
            db.session.commit()
        except Exception as e:
            print(e)
            db.sessions.rollback()
            self.assertTrue(False)


if __name__ == "__main__":
    unittest.main()
