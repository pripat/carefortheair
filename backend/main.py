from flask import Flask, jsonify, request
from db.models import Location, Bill, Charity, app, db
from flask_cors import CORS
import sqlalchemy

application = app
CORS(application, support_credentials=True)

# ---------
# Locations
# ---------


@application.route("/locations", defaults={"model_id": None}, methods=["GET"])
@application.route("/locations/<model_id>")
def get_all_locations(model_id):
    return get_all(model_id, Location)


@application.route("/locations/search")
def search_filter_sort_locations():
    result = search_filter_sort(Location)
    response = jsonify(result)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@application.route("/locations/related_bill/<bill_id>", methods=["GET"])
def get_locations_by_related_bill(bill_id):
    return get_by_related_bill(bill_id, Location)


@application.route("/locations/related_charity/<charity_id>", methods=["GET"])
def get_locations_by_related_charity(charity_id):
    return get_by_related_charity(charity_id, Location)


# -----
# Bills
# -----


@application.route("/bills", defaults={"model_id": None}, methods=["GET"])
@application.route("/bills/<model_id>")
def get_all_bills(model_id):
    return get_all(model_id, Bill)


@application.route("/bills/search")
def search_filter_sort_bills():
    result = search_filter_sort(Bill)
    response = jsonify(result)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@application.route("/bills/related_location/<location_id>", methods=["GET"])
def get_bills_by_related_location(location_id):
    return get_by_related_location(location_id, Bill)


@application.route("/bills/related_charity/<charity_id>", methods=["GET"])
def get_bills_by_related_charity(charity_id):
    return get_by_related_charity(charity_id, Bill)


# ---------
# Charities
# ---------


@application.route("/charities", defaults={"model_id": None}, methods=["GET"])
@application.route("/charities/<model_id>")
def get_all_charities(model_id):
    return get_all(model_id, Charity)


@application.route("/charities/search")
def search_filter_sort_charities():
    result = search_filter_sort(Charity)
    response = jsonify(result)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@application.route("/charities/related_location/<location_id>", methods=["GET"])
def get_charities_by_related_location(location_id):
    return get_by_related_location(location_id, Charity)


@application.route("/charities/related_bill/<bill_id>", methods=["GET"])
def get_charities_by_related_bill(bill_id):
    return get_by_related_bill(bill_id, Charity)


# ---
# All
# ---


@application.route("/all", methods=["GET"])
def get_all_models():
    result = {}
    result["locations"] = list(location.jsonify() for location in Location.query.all())
    result["bills"] = list(bill.jsonify() for bill in Bill.query.all())
    result["charities"] = list(charity.jsonify() for charity in Charity.query.all())

    response = jsonify(result)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


@application.route("/all/search", methods=["GET"])
def search_all_models():
    if request.args.get("query") is None:
        return get_all_models()

    result = {}
    result["locations"] = search_filter_sort_locations().json
    result["bills"] = search_filter_sort_bills().json
    result["charities"] = search_filter_sort_charities().json

    response = jsonify(result)
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


# ----
# Home
# ----


@application.route("/", methods=["GET"])
def index():
    return jsonify(
        {"Postman API": "https://documenter.getpostman.com/view/6780686/S11LuJqb"}
    )


# --------------
# Error Handling
# --------------


class InvalidUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv["message"] = self.message
        return rv


@application.errorhandler(InvalidUsage)
def handle_invalid_usage(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


# --------------
# Helper Methods
# --------------


def get_all(model_id, db_model):
    results = None
    if model_id is not None:
        try:
            model_id = int(model_id)
            results = db_model.query.filter_by(model_id=model_id)
        except ValueError:
            raise InvalidUsage(message="The model id must be an integer")
    else:
        results = db_model.query.all()
    return prepare_response(results)


def get_by_related_location(location_id, db_model):
    results = []
    if location_id is not None:
        try:
            results = db_model.query.filter_by(related_location_id=int(location_id))
        except ValueError:
            raise InvalidUsage(message="The location id must be an integer")
    return prepare_response(results)


def get_by_related_bill(bill_id, db_model):
    results = []
    if bill_id is not None:
        try:
            results = db_model.query.filter_by(related_bill_id=int(bill_id))
        except ValueError:
            raise InvalidUsage(message="The bill id must be an integer")
    return prepare_response(results)


def get_by_related_charity(charity_id, db_model):
    results = []
    if charity_id is not None:
        try:
            results = db_model.query.filter_by(related_charity_id=int(charity_id))
        except ValueError:
            raise InvalidUsage(message="The charity id must be an integer")
    return prepare_response(results)


def prepare_response(result):
    response = jsonify(list(element.jsonify() for element in result))
    response.headers.add("Access-Control-Allow-Origin", "*")
    return response


def shorten_context(query, context_string):
    index = context_string.lower().index(query.lower())
    lower_bound = max(0, index - 75)
    upper_bound = min(len(context_string), index + 75)
    result = context_string[lower_bound:upper_bound]
    if lower_bound != 0:
        result = result[result.find(" ") + 1 :]
        result = "..." + result
    if upper_bound != len(context_string):
        result = result[0 : result.rfind(" ")]
        result = result + "..."
    return result


def search_filter_sort(db_model):
    result = {}

    db_query = db_model.query

    db_query, result["query"] = search(db_query=db_query, db_model=db_model)

    db_query = filter(db_query=db_query, db_model=db_model)

    db_query = sort(db_query=db_query, db_model=db_model)

    result["results"] = list(element.jsonify() for element in db_query)
    result = get_search_context(search_results=result, db_model=db_model)
    return result


def search(db_query, db_model):
    query = request.args.get("query")
    if query is None:
        return db_model.query, ""
    elif query == "":
        return db_model.query.filter_by(model_id=-1), query
    else:
        search_results = search_helper(
            query_string=query, db_query=db_query, db_model=db_model
        )
        if search_results.count() == 0:
            query_tokens = query.split()
            if len(query_tokens) > 1:
                for token in query_tokens:
                    search_results = search_helper(
                        query_string=query, db_query=db_query, db_model=db_model
                    )
                    if search_results.count() > 0:
                        query = token
                        break

        return search_results, query


def search_helper(query_string, db_query, db_model):
    query_string = "%" + query_string + "%"
    results = db_model.query.filter_by(model_id=-1)
    for attribute in db_model.search_attributes:
        results = results.union(
            db_query.filter(
                sqlalchemy.cast(
                    getattr(db_model, attribute), sqlalchemy.types.String
                ).ilike(str(query_string))
            )
        )
    return results


def filter(db_query, db_model):
    for attribute in db_model.filter_attributes.keys():
        value = request.args.get(attribute)
        if value is not None:
            db_attr = getattr(db_model, db_model.filter_attributes.get(attribute))
            db_query = db_query.filter(
                sqlalchemy.cast(db_attr, sqlalchemy.types.String).ilike(str(value))
            )
    return db_query


def sort(db_query, db_model):
    sort_param = request.args.get("sort")
    if sort_param is None:
        return db_query

    sort_param = sort_param.lower()
    sort_column = None
    for attribute in db_model.sort_attributes.keys():
        if sort_param.lower() == attribute.lower():
            sort_column = getattr(db_model, db_model.sort_attributes.get(attribute))

    desc = request.args.get("desc")
    if desc == "True" or desc == "true" or desc is True:
        sort_column = sqlalchemy.desc(sort_column)

    return db_query.order_by(sort_column.nullslast())


def get_search_context(search_results, db_model):
    query = search_results.get("query")
    if query == "" or None:
        return search_results

    for element in search_results["results"]:
        search_context = {}
        for attribute in db_model.search_attributes:
            if (
                element[attribute] is not None
                and query.lower() in element[attribute].lower()
            ):
                search_context[
                    db_model.get_attribute_label(attribute)
                ] = shorten_context(query, element[attribute])
        element["search_context"] = search_context
    return search_results


if __name__ == "__main__":
    application.run(debug=True)
