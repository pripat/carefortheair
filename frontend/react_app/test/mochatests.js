import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import { expect, assert } from 'chai';
import EnzymeAdapter from 'enzyme-adapter-react-16';
import { BrowserRouter, withRouter, MemoryRouter } from 'react-router-dom' // 4.0.0

import About from '../src/components/About';
import BillCard from '../src/components/BillCard';
import CharityCard from '../src/components/CharityCard';
import LocationCard from '../src/components/LocationCard'
import User from '../src/components/User';
import ExtraInfo from '../src/components/ExtraInfo';
import Home from '../src/components/Home';
import Navigation from '../src/components/Navigation';
import Bills from '../src/pages/Bills';
import Charities from '../src/pages/Charities';
import Locations from '../src/pages/Locations';
import ContextCard from '../src/components/ContextCard';
import Search from '../src/pages/Search';
import SearchBar from '../src/components/SearchBar';
import Pagination from '../src/components/Pagination';
import FilterBar from '../src/components/FilterBar';

import Highlighter from "react-highlight-words";
import AppBar from '@material-ui/core/AppBar';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';



configure({ adapter: new EnzymeAdapter() });

describe('test <User> ', () =>{
	it('should set the user\'s name', () => {
		const wrapper = shallow(<User name="clouds wow"/>);
		expect(wrapper.prop('name')).equal("clouds wow");
	});

	it('should have style in props', () => {
		const wrapper = shallow(<User />);
		expect(Object.keys(wrapper.prop('classes'))).lengthOf(2);
	});

})

describe('test <Navigation />',  () => {
	const wrapper = mount(<BrowserRouter><Navigation /></BrowserRouter>)

	it('should have correct elements on nav bar', function () {
    	expect(wrapper.containsAllMatchingElements(['Home', 'Search', 'About'])).to.equal(true);
    });

    it('should have 6 tabs in the navbar', function () {
		expect(wrapper.find('.tab')).to.have.lengthOf(6);
	});

});

describe('test <Home />',  () => {
	const wrapper = mount(<BrowserRouter><Home /></BrowserRouter>)

	it('should have the carousel class', function () {
		expect(wrapper.find('.carousel')).to.have.lengthOf(1);
	});

	it('should have the 3 cards in card deck', function () {
		expect(wrapper.find('.card')).to.have.lengthOf(3);
	});

});

describe('test <BillCard />',  () => {
	var wrapper = shallow(<BillCard />);
	it ('should render', () => {
		assert.ok(wrapper)
	})

	it('should have style in props', () => {
		expect(Object.keys(wrapper.prop('classes'))).lengthOf(2);
	});

	wrapper = shallow(<BillCard item={{}}/>)
	it('should have 6 typography components',	() => {		
		expect(wrapper.dive().find(Typography)).to.have.lengthOf(6);
	});

	it('should have a Card Media component',	() => {
		expect(wrapper.dive().find(CardMedia)).to.have.lengthOf(1);
	});

});

describe('test <CharityCard />',  () => {
	var wrapper = shallow(<CharityCard />);
	it ('should render', () => {
		assert.ok(wrapper)
	})

	it('should have style in props', () => {
		expect(Object.keys(wrapper.prop('classes'))).lengthOf(2);
	});

	wrapper = shallow(<CharityCard item={{}}/>)
	it('should have 6 typography components',	() => {		
		expect(wrapper.dive().find(Typography)).to.have.lengthOf(6);
	});

	it('should have a Card Media component',	() => {
		expect(wrapper.dive().find(CardMedia)).to.have.lengthOf(1);
	});

});

describe('test <LocationCard />',  () => {
	var wrapper = shallow(<LocationCard />);
	it ('should render', () => {
		assert.ok(wrapper)
	})

	it('should have style in props', () => {
		expect(Object.keys(wrapper.prop('classes'))).lengthOf(2);
	});

	wrapper = shallow(<LocationCard item={{}}/>)
	it('should have 6 typography components',	() => {		
		expect(wrapper.dive().find(Typography)).to.have.lengthOf(6);
	});

	it('should have a Card Media component',	() => {
		expect(wrapper.dive().find(CardMedia)).to.have.lengthOf(1);
	});

});

describe('test <ExtraInfo />',  function() {
	it('should set commits of user', () => {
		const wrapper = shallow(<ExtraInfo commits="60"/>);
		expect(wrapper.prop('commits')).equal("60");
	});

	it('should set issues of user', () => {
		const wrapper = shallow(<ExtraInfo issues="25"/>);
		expect(wrapper.prop('issues')).equal("25");
	});

	it('should have style in props', () => {
		const wrapper = shallow(<ExtraInfo />);
		expect(Object.keys(wrapper.prop('classes'))).lengthOf(2);
	});

});

describe('test <Bills />',  () => {
	const wrapper = shallow(<Bills/>)

	it ('should render', () => {
		assert.ok(wrapper)
	})

	it('sorting in bills', function () {
		expect(wrapper.find('.sorting')).to.have.lengthOf(1);
	});

	it('should have a search bar ', function () {
		expect(wrapper.find(SearchBar)).to.have.lengthOf(1);
	});

	it('should have 4 filter fields', function () {
		expect(wrapper.find(FilterBar)).to.have.lengthOf(4);
	});

	it ('should render', () => {
		assert.ok(wrapper)
	})
});

describe('test <Charities />',  () => {
	const wrapper = shallow(<Charities/>)
	it ('should render', () => {
		assert.ok(wrapper)
	})
	it('filtering in charities', function () {
		expect(wrapper.find('.filtering')).to.have.lengthOf(1);
	});
});

describe('test <Locations />',  () => {
	const wrapper = shallow(<Locations/>)
	it ('should render', () => {
		assert.ok(wrapper)
	})
	it('sorting in locations', function () {
		expect(wrapper.find('.sorting')).to.have.lengthOf(1);
	});
});

describe('test <ContextCard />',  () => {
	const wrapper = shallow(<ContextCard />)
	it ('should render', () => {
		assert.ok(wrapper)
	})
	it('should have style in props', () => {
		expect(Object.keys(wrapper.prop('classes'))).lengthOf(4);
	});
	it('should have a highlight class',	() => {
		const wrapper = shallow(<ContextCard item={{'short_title': "h", "title":"a", "search_context":{}}}/>)
		expect(wrapper.dive().find(Highlighter)).to.have.lengthOf(2);
	});

	
});

describe('test <Search />',  () => {
	const wrapper = shallow(<Search />)
	it('should have a searchbar component', () => {
		expect(wrapper.contains(<SearchBar/>));
	});

	it('should have a searchbar field', function () {
		expect(wrapper.dive().find(SearchBar)).to.have.lengthOf(1);
	});

	it ('should render', () => {
		assert.ok(wrapper)
	})

});

describe('test <SearchBar />',  () => {
	const wrapper = shallow(<SearchBar />)

	it('should have an input field', function () {
		expect(wrapper.find('.input')).to.have.lengthOf(1);
	});

	it('should have a button', function () {
		expect(wrapper.find('.button')).to.have.lengthOf(1);
	});

  	it('should be able to set text entered', () => {
	    const instance = wrapper.instance();
	    instance.state.textValue = "hello";
	    expect(Object.values(instance.state)).to.include("hello");
  	});

});

describe('tests for <Pagination> component', () => {
  	const wrapper = shallow(<Pagination totalRecords={100} pageLimit={15} />);
  	it('should include name pagination for nav', () => {
    	expect(Object.keys(wrapper.props().children.props)).includes('aria-label','name');
  	});

  	it('should update current page in state', () => {
	    const instance = wrapper.instance();
	    instance.gotoPage(4);
	    expect(Object.values(instance.state)).to.include(4);
  	});
});

describe('test <FilterBar />',  () => {
	const wrapper = shallow(<FilterBar name="sort" menu={['a','b','c']}/>)

	it('should have required props', function () {
		expect(Object.keys(wrapper.props())).includes('name','menu','filterBy');
	});

	it('should have an form field', function () {
		expect(wrapper.dive().find("form")).to.have.lengthOf(1);
	});

});

