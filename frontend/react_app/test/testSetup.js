// Mocha can't handle .css , .png, .jpg
require.extensions['.css'] = function () {
  return null;
};
require.extensions['.png'] = function () {
  return null;
};
require.extensions['.jpg'] = function () {
  return null;
};
require.extensions['.mp4'] = function () {
  return null;
};

require('isomorphic-fetch');
