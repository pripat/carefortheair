import React, { Component } from 'react';
import './App.css';
import Navigation from './components/Navigation'
import Home from './components/Home'
import About from './components/About'
import Locations from './pages/Locations'
import LocationInstance from './pages/LocationInstance'
import Charities from './pages/Charities'
import CharityInstance from './pages/CharityInstance'
import Bills from './pages/Bills'
import BillInstance from './pages/BillInstance'
import Search from './pages/Search'
import Visualizations from './pages/Visualizations'

import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  typography: {
    fontFamily: 'Avenir',
    useNextVariants: true
  }
});

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
      <Router>
        <div>
          <Navigation />
          <Switch>
              <Route exact path="/">
                <Redirect to="/Home" />
              </Route>
              <Route exact path='/Home' component={Home} />
              <Route exact path='/About' component={About} />
              <Route exact path='/Locations' component={Locations} />
              <Route exact path='/Charities' component={Charities} />
              <Route exact path='/Bills' component={Bills} />
              <Route path='/Locations/:id' component={LocationInstance} />
              <Route path='/Charities/:id' component={CharityInstance} />
              <Route path='/Bills/:id' component={BillInstance} />
              <Route exact path='/Search' component={Search} />
              <Route exact path='/Visualizations' component={Visualizations} />
          </Switch>
        </div>
      </Router>
      </MuiThemeProvider>
    );
  }
}

export default App;
