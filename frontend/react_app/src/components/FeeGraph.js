import React, {Component} from 'react';
import axios from 'axios';
import * as d3 from "d3";
import './tip.css'

class FeeGraph extends Component {
  constructor(props) {
    super(props);
    this.income_data = {}
    this.state = {
        didLoad: false
    };
    this.data = []
  }

  componentDidMount() {
    this.getFeeInfo();
  }

  componentDidUpdate(prevProps, prevState) {
        const didLoad = this.state.didLoad;
        if (didLoad !== prevState.didLoad) {
            this.drawChart();
        }
    }

  getFeeInfo() {
    axios.all([axios.get("https://api.connectwithnature.me/state")]).then(
      axios.spread(result => {
          for (var i = 0; i < result.data.length; i++) {
            const incomeRank = parseInt(result.data[i].incomeRank.replace("th", ""))

            this.income_data[result.data[i].abbr] = incomeRank
          }
          console.log("data", this.income_data)
          this.getParkInfo()
      })
    );
  }

  getParkInfo() {
    axios.all([axios.get("https://api.connectwithnature.me/park")]).then(
      axios.spread(result => {
        for (const key in this.income_data) {
          var total_fees = 0;
          var num_fees = 0;
          for (var i = 0; i < result.data.length; i++) {
            if (result.data[i].states === key) {
              total_fees += result.data[i].fee
              num_fees += 1
            }
          }
          if (total_fees > 0) {
            this.data.push({state : key, rank: this.income_data[key], fee: total_fees/num_fees})
          }
        }
        console.log("data", this.data)
        this.data = this.data.sort(function(a,b) {
          return a.rank - b.rank
        })

        this.setState({
          didLoad: true
        });
      })
    );
  }

  drawChart() {
    var width = 960;
    var height = 700;
    var padding = 70;

    const margin = 75;

    //Create scale functions
    var xScale = d3.scaleLinear()
                .domain([0, d3.max(this.data, d => d.rank)]).nice()
                .range([padding, width - padding * 2]);

    var yScale = d3.scaleLinear()
                .domain([0, d3.max(this.data, d => d.fee)]).nice()
                .range([height - padding, padding]);
    //Define X axis
    var xAxis = d3.axisBottom(xScale)
                .ticks(4)


    //Define Y axis
    var yAxis = d3.axisLeft(yScale)

    var area = d3.area()
      .x(d => xScale(d.rank))
      .y0(yScale(0))
      .y1(d => yScale(d.fee))

    var svg = d3.select("#line-graph")
        .append("svg")
        .attr("width", width)
        .attr("height", height)
        .attr('transform', `translate(${margin}, ${margin})`);

    svg.append("path")
      .datum(this.data)
      .attr("fill", "#7fa46a")
      .attr("d", area);
      
    svg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + (height - padding) + ")")
        .call(xAxis);
    
    svg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(" + padding + ",0)")
        .call(yAxis);

    svg.append('text')
        .attr('x', -(height / 2))
        .attr('y', margin / 5)
        .attr('transform', 'rotate(-90)')
        .attr('text-anchor', 'middle')
        .text('Average Park Fee');

      svg.append('text')
        .attr('x', width / 2)
        .attr('y', height - margin / 3)
        .attr('text-anchor', 'middle')
        .text('States Income Rank');

    d3.select("#line-graph")
        .attr("width", "100%")
        .attr("display", "block");
} 
        
  render(){
    
    if(this.didLoad) {
      this.drawChart();
    }

    return (
        <div >
            <svg id="line-graph" width="960" height="700" viewBox="0 0 960 700" preserveAspectRatio="xMidYMid meet"></svg>
        </div>
    );
  }
}
    
export default FeeGraph;