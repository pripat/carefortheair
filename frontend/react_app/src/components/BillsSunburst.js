import React, { Component } from "react";
import axios from 'axios';
import * as d3 from "d3";
import bills_data from "./data/bills_data.json";

class BillsSunburst extends Component {
    constructor(props) {
        super(props);
        this.data = {};
        this.state = {
            didLoad: false
        };
    }

    componentDidMount() {
        this.getInfo();
    }

    componentDidUpdate(prevProps, prevState) {
        const didLoad = this.state.didLoad;
        if (didLoad !== prevState.didLoad) {
            // console.log("Data", this.data);
            this.createSunburst();
        }
    }

    getInfo() {
        axios.all([axios.get("https://api.carefortheair.me/bills")]).then(
            axios.spread(result => {
                this.data = result.data;
                this.setState({
                    didLoad: true
                });
            })
        );
    }

    createSunburst() {
        var width = 800;
        var height = 800;
        var radius = Math.min(width, height) / 2;

        var g = d3.select("#sunburst")
            .attr("width", width + "px")
            .attr("height", height + "px")
            .append("g")
            .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

        var partition = d3.partition()
            .size([2 * Math.PI, radius]);

        var rootNode = d3.hierarchy(bills_data)
            .sum(function(data) {
                return data.size;
            });

        partition(rootNode);
        var arc = d3.arc()
            .startAngle(function(data) { return data.x0; })
            .endAngle(function(data) { return data.x1; })
            .innerRadius(function(data) { return data.y0; })
            .outerRadius(function(data) { return data.y1; });

        g.selectAll("g")
            .data(rootNode.descendants())
            .enter()
            .append("g")
            .attr("class", "node")
            .append("path")
            .attr("display", function (data) {
                return data.depth ? null : "none";
            })
            .attr("d", arc)
            .style("stroke", "#fff")
            .style("fill", function (data) {
                if (data.parent === null) {
                    return "#000";
                } else if (data.parent.parent === null) {
                    const party = data.data.name;
                    if (party === "Democratic") {
                        return "#0000ff";
                    } else {
                        return "#ff0000";
                    }
                } else if (data.parent.parent.parent === null) {
                    const party = data.parent.data.name;
                    if (party === "Democratic") {
                        return "#4c4cff";
                    } else {
                        return "#ff4c4c";
                    }
                } else if (data.parent.parent.parent.parent === null) {
                    const party = data.parent.parent.data.name;
                    if (party === "Democratic") {
                        return "#a2a2f9";
                    } else {
                        return "#f9a2a2";
                    }
                } else {
                    return "#000";
                }
            });

        g.selectAll(".node")
            .append("text")
            .attr("transform", function(data) {
                return "translate(" + arc.centroid(data) + ")rotate(" + computeTextRotation(data) + ")";
            })
            .attr("x", 0)
            .attr("dx", -20)
            .style("font-size", "10px")
            .attr("fill-opacity", function(data) {
                // console.log("Data", data);
                const result = (data.y1 - data.y0 >= 62.5) && (data.x1 - data.x0 !== 0);

                if (result) {
                    return 1;
                } else {
                    return 0;
                }
            })
            .datum(function(data) {
                // console.log("Name", data.data.name);
                return data.parent ? data.data.name : "";
            })
            .append("tspan")
            .attr("x", 0)
            .attr("dx", -20)
            .attr("dy", -10);

        g.selectAll("tspan")
            .datum(function(data) {
                return data.split(",");
            })
            .append("tspan")
            .attr("x", 0)
            .attr("dx", -40)
            .attr("dy", -5)
            .text(function(data) {
                return data[0];
            })
            .append("tspan")
            .attr("x", 0)
            .attr("dx", -40)
            .attr("dy", 10)
            .text(function(data) {
                if (data[1] !== undefined) {
                    return data[1];
                }
                return "";
            })
            .append("tspan")
            .attr("x", 0)
            .attr("dx", -40)
            .attr("dy", 10)
            .text(function(data) {
                if (data[2] !== undefined) {
                    return data[2];
                }
                return "";
            });

        d3.select("#sunburst")
            .attr("width", "100%")
            .attr("display", "block");

        function computeTextRotation(data) {
            var angle = (data.x0 + data.x1) / Math.PI * 90;
            return (angle < 180) ? angle - 90 : angle + 90;
        }
    }

    render() {
        if (this.state.didLoad) {
            // console.log("Data", this.state.data);
            this.createSunburst();
        }
        return(
            <svg id="sunburst" style={{overflow:"visible"}} viewBox="0 0 800 800" preserveAspectRatio="xMidYMid meet"></svg>
        );
    }
}

export default BillsSunburst;
