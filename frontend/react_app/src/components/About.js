import React from 'react';
import User from './User.js'
import ExtraInfo from './ExtraInfo.js'
import axios from 'axios';
import lauren from './assets/Lauren.jpg';
import maria from './assets/Maria.jpg';
import priya from './assets/Priya.jpg';
import baonhi from './assets/BaoNhi.jpg';
import brandon from './assets/Brandon.jpg';

class About extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            commits: {"Lauren Doan" : 0, "mariaocanas" : 0, "Priya Patel" : 0, "Bao Nhi Thai" : 0, "Brandon Wright" : 0},
            issues: {"Lauren Doan" : 0, "Maria Ocanas" : 0, "Priya Patel" : 0, "Bao Nhi Thai" : 0, "Brandon Wright" : 0},
            totalCommits: 0,
            totalIssues: 0,
        }
  }

  componentDidMount() {
    let thisComponent = this;

    var i;
    for (i = 1; i < 8; i++){
      axios.get('https://gitlab.com/api/v4/projects/11049634/repository/commits?per_page=100&page='.concat(i))
        .then(result => {
          result.data.forEach (commit => {
            let teamMember = commit.author_name
            let userCommits = thisComponent.state["commits"]
            userCommits[teamMember] += 1;
            let total = thisComponent.state["totalCommits"]
            total += 1;
            this.setState({
              userCommits: userCommits,
              totalCommits: total,
            })
          })
        });
    }

    for (i = 1; i < 3; i++) {
      axios.get('https://gitlab.com/api/v4/projects/11049634/issues?per_page=100&page='.concat(i))
        .then(result => {
          result.data.forEach (issue => {
            if (issue.closed_by !== null) {
              let teamMember = issue.closed_by.name
              let userIssues = thisComponent.state["issues"]
              userIssues[teamMember] += 1
              let total = thisComponent.state["totalIssues"]
              total += 1
              this.setState({
                issues: userIssues,
                totalIssues: total
              })
            } else {
              let total = thisComponent.state["totalIssues"]
              total += 1
              this.setState({
                totalIssues: total
              })
            }
          })
        });
    }
  }

  render() {
    const { commits, issues, totalCommits, totalIssues } =  this.state;
    return (
      <div>
        <div className='container'>
          <br />
          <h1>Our goal</h1>
          <p>The motivation behind this project is to promote civic engagement by raising awareness about air pollution and its consequences. As environmental concerns and climate change become more prevalent, it is important that the population is more informed about the different ways that they can contribute to environmental sustainability. We hope that the information presented on our website will encourage more people to engage with their representatives in Congress or nearby charities in order to improve air quality and the environment overall.</p>
        </div>
        <div className='container'>
          <div className="row mb-5 mt-5">
            <div className="col-sm-4 mt-3">
              <User
                image={lauren}
                name="Lauren Doan"
                bio="4th year computer science major who likes eating"
                responsibilities="front-end developer"
                user="Lauren Doan"
                commits={commits}
                issues={issues} />
            </div>
            <div className="col-sm-4 mt-3">
              <User
                image={maria}
                name="Maria Ocanas"
                bio="4th year computer science major who likes watching YouTube religiously and running"
                responsibilities="back-end developer"
                user="mariaocanas"
                commits={commits}
                issues={issues} />
            </div>
            <div className="col-sm-4 mt-3">
              <User
                image={priya}
                name="Priya Patel"
                bio="4th year computer science major who has way too much to do and not enough time"
                responsibilities="front-end developer"
                user="Priya Patel"
                commits={commits}
                issues={issues} />
            </div>
          </div>
          <div className="row" >
            <div className="col-sm-4 mt-3">
              <User
                image={baonhi}
                name="Bao Nhi Thai"
                bio="4th year computer science and chinese majors who likes fried chicken and traveling"
                responsibilities="front-end developer"
                user="Bao Nhi Thai"
                commits={commits}
                issues={issues} />
            </div>
            <div className="col-sm-4 mt-3">
              <User
                image={brandon}
                name="Brandon Wright"
                bio="4th year computer science major who likes reality TV and graham crackers"
                responsibilities="back-end developer"
                user="Brandon Wright"
                commits={commits}
                issues={issues} />
            </div>
            <div className="col-sm-4 mt-3">
              <ExtraInfo
                commits={totalCommits}
                issues={totalIssues} />
            </div>
          </div>
        </div>

        <div className="container mt-5">
          <h1>Tools</h1>
          <p>
            <a href="https://gitlab.com/">Gitlab:</a> Used for version control and issue tracking. <br />
            <a href="https://www.getpostman.com/">Postman:</a> Used to design and our API. <br />
            <a href="https://aws.amazon.com/">AWS:</a> Used S3 with Cloudfront for hosting our static site, ACM for provisioning our SSL certificate, RDS to create a Postgres database, and Elastic Beanstalk to deploy Flask app. <br />
            <a href="https://react-bootstrap.github.io/">React Bootstrap:</a> Used to make the Bootstrap stylesheet into React components so we can use Bootstrap styling without bootstrap.js or jQuery. <br />
            <a href="https://reacttraining.com/react-router/web/guides/quick-start">React-Router:</a> Used to navigate between pages through the navigation bar and links on each page. <br />
            <a href="https://github.com/axios/axios">Axios:</a> Used to make HTTP get requests to GitLab and our API in order to make website dynamic. <br />
            <a href="https://www.namecheap.com/">NameCheap:</a> Used to get a custom domain name and connect it to our Cloudfront instance. <br />
            <a href="https://material-ui.com/">Material-UI:</a> Used for styling, dynamically works with the back-end side. <br />
            <a href="http://flask.pocoo.org/">Flask:</a> Used to implement the backend server.<br />
            <a href="https://www.sqlalchemy.org/">SQLAlchemy:</a> Used to create and update the tables with our model instance data <br />
            <a href="https://mochajs.org/">Mocha:</a> Used to write unit tests for the frontend JavaScript code. <br />
            <a href="https://www.seleniumhq.org/">Selenium:</a> Used to write acceptance tests for the GUI. <br />
            <a href="http://plantuml.com/">PlantUML:</a> Used to create a UML diagram. <br />
            <a href="https://d3js.org/">D3:</a> Used to create data visualization. <br />
          </p>
        </div>
        <footer className="page-footer font-small blue pt-4">
            <div className="footer-copyright text-center py-3">© 2019 Copyright: Clouds. wow.
            </div>
        </footer>
      </div>
    )
  }
}

export default About;
