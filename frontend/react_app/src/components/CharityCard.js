import React from 'react';
import { Link } from 'react-router-dom';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    maxWidth: '100%',
  },
  media: {
    objectFit: 'contain',
  },
};


const CharityCard = (props) => {
	const { classes } = props;

	return(
		<div>
		<Card className={classes.card} name="charity-card">
			<CardActionArea
				component = {Link} to = {"/Charities/" + props.item.model_id} style={{textDecoration: "none"}}>
				<CardMedia
					component = "img"
					className= {classes.media}
					height="300"
					image = {props.item.charity_photo}
					title =""/>
					<CardContent>
						<Typography gutterBottom variant="h5" component="h2">
							{props.item.name}
						</Typography>
						<Typography component="p">
				            <b>State: </b> {props.item.state}
				        </Typography>
				        <Typography component="p">
				            <b>City: </b> {props.item.city}
				        </Typography>

				        <Typography component="p">
				            <b>Category: </b> {props.item.category}
				        </Typography>
				        <Typography component="p">
				            <b>Classification: </b> {props.item.ntee_classification}
				        </Typography>
				        <Typography component="p">
				            <b>Rating: </b> <img src ={props.item.rating_image} alt=""/>
				        </Typography>
				        </CardContent>
			</CardActionArea>
		</Card>
		</div>
	)
}

CharityCard.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(CharityCard);
