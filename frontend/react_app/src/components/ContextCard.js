import React, {Component} from 'react';
import { Link } from 'react-router-dom';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Highlighter from "react-highlight-words";

const styles = {
  card: {
    maxWidth: '100%',
    display: 'flex'
  },
  media: {
  	objectFit: 'contain',
    width: 150,
    maxHeight: 150,
    paddingLeft: "10px"
  },
  details: {
    flexDirection: 'column',
  },
  content: {
    flex: '1 0 auto',
  }
};


class ContextCard extends Component{
	state = {
	}

	constructor(props){
		super(props)
		this.image = null
		this.extension = ""
		this.cardName = ""
		this.title = ""
		this.query = ""
	}

	render(){
		const { classes } = this.props;
		var contextListKey = Object.keys(this.props.item.search_context)
		var contextKey = contextListKey[0]
		var context = this.props.item.search_context[contextKey]
		var queryArray = []
		queryArray.push(this.props.query)
		var title;
		var image;

		if(this.props.cardName === "bill-context-card"){

	        if (this.props.item.short_title === this.props.item.title || this.props.item.short_title == null) {
	            title = this.props.item.bill_number
	        } else {
	            title = this.props.item.short_title
	        }
		}
		else{
			title = this.props.title
		}

		if(this.props.cardName === "location-context-card"){
			if (this.props.item.wiki_image) {
				image = this.props.item.wiki_image
			} else {
		    	image = "https://www.metoffice.gov.uk/binaries/content/gallery/metofficegovuk/hero-images/weather/cloud/cumulonimbus-2.jpg/cumulonimbus-2.jpg/metofficegovuk%3AheroMedium"
			}
		}
		else{
			image = this.props.image
		}
		


		return(
			<div>
			<CardActionArea component = {Link} to = {this.props.extension + this.props.item.model_id} style={{textDecoration: "none"}}>
				<Card className={classes.card} name={this.props.cardName}>
					<CardMedia
						component = "img"
						className= {classes.media}
						image = {image}
						title =""
					/>
					<div className={classes.details}>
						<CardContent className={classes.content}>
							<Typography gutterBottom variant="h5" component="h2">
							<Highlighter
				                name= "highlight"
				                highlightClassName="YourHighlightClass"
				                searchWords={queryArray}
				                autoEscape={true}
				                textToHighlight={title}
				            />
							</Typography>
							<Typography component="p">
							<b>{contextKey}: </b>
				            <Highlighter
				            	name= "highlight"
					            highlightClassName="YourHighlightClass"
					            searchWords={queryArray}
					            autoEscape={true}
					            textToHighlight={context}
				            />
					        </Typography>
					    </CardContent>
					</div>
				</Card>
			</CardActionArea>
			</div>
		)
	}
}

ContextCard.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ContextCard);
