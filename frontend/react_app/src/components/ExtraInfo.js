import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    maxWidth: "100%",
    maxHeight: "100%"
  },
  media: {
    width: 50,

  },
};




const ExtraInfo = ({ commits, issues }) =>
  (
    <div>
    <Card>

        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
              <b>Totals: </b>
          </Typography>
          <Typography component="p">
              <b>Total commits: </b> {commits}
          </Typography>
          <Typography gutterBottom component="p">
              <b>Total issues: </b> {issues}
              <br />
          </Typography>

          <Typography gutterBottom variant="h5" component="h2">
              <br />
              <b>Links: </b>
          </Typography>

          <Typography component="p" align="left">
            <a href="https://gitlab.com/pripat/carefortheair"> <strong>GitLab Repo</strong>
                                </a>
                                <br />
            <a href="https://documenter.getpostman.com/view/6780686/S11LuJqb">
                                   <strong>Postman</strong>
                                </a>

          </Typography>
          <Typography gutterBottom variant="h5" component="h2">
              <br />
              <b>Data Sources: </b>
          </Typography>

          <Typography component="p" align="left">
            <a href="https://charity.3scale.net/"> <strong>Charity API</strong>
                                </a>
                                <br />
            <a href="https://projects.propublica.org/api-docs/congress-api/bills/#get-a-specific-bill-subject">
                                   <strong>Bills API</strong>
                                </a>
                                <br />
            <a href="http://aqicn.org/api/">
                                   <strong>Air Quality API</strong>
                                </a>
                                <br />
            <a href="https://www.mediawiki.org/wiki/API:Main_page">
                                   <strong>Wikipedia API</strong>
                                </a>
                                <br />
            <a href="https://developers.google.com/maps/documentation/geocoding/start">
                                   <strong>Google Geocoding API</strong>
                                </a>
                                <br />
            <a href="https://public.opendatasoft.com/explore/dataset/worldcitiespop/api/?disjunctive.country">
                                   <strong>World Cities Population API</strong>
                                </a>
                                <br />
            <a href="https://developers.google.com/youtube/v3/">
                                   <strong>YouTube Data API</strong>
                                </a>
                                <br />

          </Typography>
        </CardContent>
    </Card>
    </div>
  )

export default withStyles(styles)(ExtraInfo);
