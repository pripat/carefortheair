import React from 'react';

import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const users = {
    "Lauren Doan": "Lauren Doan",
    "mariaocanas": "Maria Ocanas",
    "Priya Patel": "Priya Patel" ,
    "Bao Nhi Thai": "Bao Nhi Thai", 
    "Brandon Wright": "Brandon Wright"
};

const unitTests = {
    "Lauren Doan": 7,
    "mariaocanas": 5,
    "Priya Patel": 35,
    "Bao Nhi Thai": 3,
    "Brandon Wright": 55
};


const styles = {
  card: {
    maxWidth: "100%"
  },
  media: {
    height: 300,
    objectFit: 'cover',
  },
};

const User = ({ image, name, bio, responsibilities, user, commits, issues }) =>
  (
    <div>
    <Card name="user-card">
      <CardMedia 
        component = "img"
        image = {image}

        title = {name} />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
              {name}
          </Typography>
          <Typography component="p">
              <b>Bio: </b> {bio}
          </Typography>
          <Typography component="p">
              <b>Major responsibilities: </b> {responsibilities}
          </Typography>
          <Typography component="p" name="commits">
              <b>Number of commits: </b> {commits[user]}
          </Typography>
          <Typography component="p" name="issues">
              <b>Number of issues closed: </b> {issues[users[user]]}
          </Typography>
          <Typography component="p">
              <b>Number of unit tests: </b> {unitTests[user]}
          </Typography>
        </CardContent>
    </Card>
    </div>
  )

export default withStyles(styles)(User);
