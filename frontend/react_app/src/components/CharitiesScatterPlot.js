import React, {Component} from 'react';
import axios from 'axios';
import * as d3 from "d3";
import './tip.css'

class CharitiesScatterPlot extends Component {
  constructor(props) {
    super(props);
    this.data = []
    this.state = {
        didLoad: false
    };
  }

  componentDidMount() {
    this.getCharityInfo();
  }

  componentDidUpdate(prevProps, prevState) {
        const didLoad = this.state.didLoad;
        if (didLoad !== prevState.didLoad) {
            this.drawChart();
        }
    }

  getCharityInfo() {
    axios.all([axios.get("https://api.carefortheair.me/charities")]).then(
      axios.spread(result => {
              for (var i = 0; i < result.data.length; i++) {
                const rating = result.data[i].rating
                const income = result.data[i].income_amount / 1000000
                var data = []
                data.push(rating)
                data.push(income)
                this.data.push(data)
              }
              this.setState({
                didLoad: true
              });
            })
        );
  }

  drawChart() {
      //Width and height
      var width = 1000;
      var height = 700;
      var padding = 50

      const margin = 90;
      width = width - margin * 2
      height = height - margin * 2

      //Create scale functions
      var xScale = d3.scaleLinear()
                  .domain([0, d3.max(this.data, function(d) { return d[0]; })])
                  .range([padding, width - padding * 2]);

      var yScale = d3.scaleLinear()
                  .domain([0, d3.max(this.data, function(d) { return d[1]; })])
                  .range([height - padding, padding]);

      var rScale = d3.scaleLinear()
                  .domain([0, d3.max(this.data, function(d) { return d[1]; })])
                  .range([5, 15]);

      var colorScale = d3.scaleOrdinal(d3.schemePuBu[7])
                  .domain([0, d3.max(this.data, function(d) { return d[1]; })])


      //Define X axis
      var xAxis = d3.axisBottom(xScale)
                  .ticks(4)

      //Define Y axis
      var yAxis = d3.axisLeft(yScale)

      //Create SVG element
      var svg = d3.select("body")
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .attr('transform', `translate(${margin}, ${margin})`);

      svg.selectAll("circle")
         .data(this.data)
         .enter()
         .append("circle")
         .attr("cx", function(d) {
            return xScale(d[0]);
         })
         .attr("cy", function(d) {
            return yScale(d[1]);
         })
         .attr("r", function(d) {
            return rScale(d[1]);
         })
         .style("fill", function(d) { 
            return colorScale(d[0]); 
         });

      //Create X axis
      svg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + (height - padding) + ")")
        .call(xAxis);
      
      //Create Y axis
      svg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(" + padding + ",0)")
        .call(yAxis);

      svg.append('text')
        .attr('x', -(height / 2))
        .attr('y', margin / 5)
        .attr('transform', 'rotate(-90)')
        .attr('text-anchor', 'middle')
        .text('Income Amount (Millions of $)')

      svg.append('text')
        .attr('x', width / 2 + margin)
        .attr('y', height)
        .attr('text-anchor', 'middle')
        .text('Charity Rating')
  }
        
  render(){
    if(this.didLoad) {
      this.drawChart();
    }
    return <svg style={{overflow:"visible"}} id="scatter-plot" width="960" height="700"></svg>
  }
}
    
export default CharitiesScatterPlot;