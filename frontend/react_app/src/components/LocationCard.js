import React from 'react';
import { Link } from 'react-router-dom';

import good from '../assets/good.png';
import moderate from '../assets/moderate.png';
import sensitive from '../assets/sensitive.png';
import unhealthy from '../assets/unhealthy.png';
import veryunhealthy from '../assets/veryunhealthy.png';
import hazardous from '../assets/hazardous.png';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';


const styles = {
  card: {
    maxWidth: '100%',
  },
  media: {
    objectFit: 'cover',
  },
};


const LocationCard = (props) => {
	const { classes } = props;

	var image;
	if (props.item.wiki_image) {
		image = props.item.wiki_image
	} else {
    	image = "https://www.metoffice.gov.uk/binaries/content/gallery/metofficegovuk/hero-images/weather/cloud/cumulonimbus-2.jpg/cumulonimbus-2.jpg/metofficegovuk%3AheroMedium"
	}

	var pop;
	if (props.item.population_string) {
		pop = props.item.population_string
	} else {
    	pop = "N/A"
	}

	var statusImage;
	let aqi = props.item.aqi
	if (aqi < 50) {
		statusImage = good
	}
	else if (aqi < 100) {
    	statusImage = moderate
	}
	else if (aqi < 150) {
    	statusImage = sensitive
	}
	else if (aqi < 200) {
    	statusImage = unhealthy
	}
	else if (aqi < 300) {
    	statusImage = veryunhealthy
	}
	else {
		statusImage = hazardous
	}

	return(
		<div>
		<Card className={classes.card} name="location-card">
			<CardActionArea
				component = {Link} to = {"/Locations/" + props.item.model_id} style={{textDecoration: "none"}}>
				<CardMedia
					component = "img"
					className= {classes.media}
					height="300"
					image = {image}
					title =""/>
					<CardContent>
						<Typography gutterBottom variant="h5" component="h2">
							{props.item.city_name}
						</Typography>
						<Typography component="p">
				            <b>State: </b> {props.item.state}
				        </Typography>
				        <Typography component="p">
				            <b>Population: </b> {pop}
				        </Typography>
				        <Typography component="p">
				            <b>Air Quality Index: </b> {props.item.aqi}
				        </Typography>
				        <Typography component="p">
				            <b>Dominant Pollutant: </b> {props.item.dominant_pol}
				        </Typography>
				        <Typography component="p">
				            <b>Air Status:  </b> {props.item.air_status} <br/><img src = {statusImage} width = "100" alt=""/>
				        </Typography>
				    </CardContent>
			</CardActionArea>
		</Card>
		</div>
	)
}

LocationCard.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(LocationCard);
