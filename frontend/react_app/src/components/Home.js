import React, { Component } from "react";
import { Carousel, Container } from "react-bootstrap";
import { CardDeck, Card } from 'react-bootstrap'
import one from "./assets/city.jpg"
import two from "./assets/cloud.jpg"
import three from "./assets/pollution.jpg"
import clouds from "./assets/clouds.mp4"

class Home extends Component {
  constructor(props, context) {
    super(props, context);

    this.handleSelect = this.handleSelect.bind(this);

    this.state = {
      index: 0,
      direction: null
    };

  }

  handleSelect(selectedIndex, e) {
    this.setState({
      index: selectedIndex,
      direction: e.direction
    });
  }

  render() {
    const { index, direction } = this.state;
    return (
      <div>
      <Carousel
        activeIndex={index}
        direction={direction}
        onSelect={this.handleSelect}
      >
        <Carousel.Item >
            <video className="img-fluid"
              src={clouds}
              width={2000}
              height={670}
              preload="auto"
              autoPlay loop muted/> 
            <Carousel.Caption>
                  <h2 style={{fontSize:"350%", color: "white"}}>Care For The Air</h2>
            </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img className="img-fluid"
            alt = ""
            width={2000}
            height={500}
            src={one} />
          <Carousel.Caption>
                <h2 style={{fontSize:"150%", color: "white"}}>CO<sub>2</sub> emissions are key sources of both climate change and air pollution.</h2>
                <a href="https://www.iass-potsdam.de/en/output/dossiers/air-pollution-and-climate-change" rel="noopener noreferrer" target="_blank">
                  <h6 style={{color:"white"}}>(Click for more info.)</h6>
                </a>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img className="img-fluid"
            alt = ""
            width={2000}
            height={500}
            src={two} />
          <Carousel.Caption>
            <h2 style={{fontSize:"150%", color:"white"}}>92% of the world lives in places with unhealthy air quality.</h2>
            <a href="http://web.unep.org/youngchampions/blog/get-facts-6-things-you-need-know-about-air-pollution" rel="noopener noreferrer" target="_blank">
              <h6 style={{color:"white"}}>(Click for more info.)</h6>
            </a>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img className="img-fluid"
            alt = ""
            width={2000}
            height={500}
            src={three} />
          <Carousel.Caption>
            <h2 style={{fontSize:"150%",color:"white"}}>About 4.2 million deaths every year are a result of air pollution.</h2>
            <a href="https://www.who.int/airpollution/en/" rel="noopener noreferrer" target="_blank">
              <h6 style={{color:"white"}}>(Click for more info.)</h6>
            </a>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
      <div>
      <b> </b> <br />
      <b> </b> <br />
      </div>
    <Container>
      <CardDeck>
        <Card className="card-link" name='locations-card'>
        <Card.Link href="/Locations">
          <Card.Img variant="top" src="https://images.unsplash.com/photo-1500021804447-2ca2eaaaabeb?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80"
          />
        </Card.Link>
          <Card.Body>
            <Card.Title>Locations</Card.Title>
          </Card.Body>
        </Card>
        <Card name='charities-card'>
        <Card.Link href="/Charities">
          <Card.Img variant="top" src="https://images.unsplash.com/photo-1543414993-6dbd67322ae3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
          />
        </Card.Link>
          <Card.Body>
            <Card.Title>Charities</Card.Title>
          </Card.Body>
        </Card>
        <Card name='bills-card'>
          <Card.Link href="/Bills">
          <Card.Img variant="top" src="https://images.unsplash.com/photo-1496144300411-8dd31ce145ba?ixlib=rb-1.2.1&auto=format&fit=crop&w=1372&q=80"
          />
          </Card.Link>
          <Card.Body>
            <Card.Title>Bills</Card.Title>
          </Card.Body>
        </Card>
      </CardDeck>
      <footer className="page-footer font-small blue pt-4">
          <div className="footer-copyright text-center py-3">© 2019 Copyright: Clouds. wow.
          </div>
        </footer>
      </Container>
      </div>
    );
  }
}

export default Home;
