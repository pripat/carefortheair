import React, {Component} from 'react';
import axios from 'axios';
import * as d3 from "d3";
import './tip.css';

class CampgroundsScatterPlot extends Component {
  constructor(props) {
    super(props);
    this.data = {
      "HI" : [0, 0], "AK" : [0, 0], "FL" : [0, 0], "SC" : [0, 0], "GA" : [0, 0],
      "AL" : [0, 0], "NC" : [0, 0], "TN" : [0, 0], "RI" : [0, 0], "CT" : [0, 0],
      "MA" : [0, 0], "ME" : [0, 0], "NH" : [0, 0], "VT" : [0, 0], "NY" : [0, 0],
      "NJ" : [0, 0], "PA" : [0, 0], "DE" : [0, 0], "MD" : [0, 0], "WV" : [0, 0],
      "KY" : [0, 0], "OH" : [0, 0], "MI" : [0, 0], "WY" : [0, 0], "MT" : [0, 0],
      "ID" : [0, 0], "WA" : [0, 0], "TX" : [0, 0], "CA" : [0, 0], "AZ" : [0, 0],
      "NV" : [0, 0], "UT" : [0, 0], "CO" : [0, 0], "NM" : [0, 0], "OR" : [0, 0],
      "ND" : [0, 0], "SD" : [0, 0], "NE" : [0, 0], "IA" : [0, 0], "MS" : [0, 0],
      "IN" : [0, 0], "IL" : [0, 0], "MN" : [0, 0], "WI" : [0, 0], "MO" : [0, 0],
      "AR" : [0, 0], "OK" : [0, 0], "KS" : [0, 0], "LA" : [0, 0], "VA" : [0, 0]
    };
    this.points = [];
    this.states = [];
    this.state = {
        didLoad: false
    };
  }

  componentDidMount() {
    this.getDensityInfo();
  }

  componentDidUpdate(prevProps, prevState) {
        const didLoad = this.state.didLoad;
        if (didLoad !== prevState.didLoad) {
            // console.log("Data", this.data);
            // console.log("Points", this.points);
            // console.log("States", this.states);
            this.drawChart();
        }
    }

  getDensityInfo() {
    axios.all([axios.get("https://api.connectwithnature.me/state")]).then(
      axios.spread(result => {
          for (var i = 0; i < result.data.length; i++) {
            const population = parseFloat(result.data[i].population.replace(/,/g, ''))
            const area = parseFloat(result.data[i].area.replace(/,/g, ''))
            var density = parseFloat((population / area).toFixed(2))
            this.data[result.data[i].abbr][0] = density
          }
          this.getParkInfo()
      })
    );
  }

  getParkInfo() {
    axios.all([axios.get("https://api.connectwithnature.me/park")]).then(
      axios.spread(result => {
        for (const key in this.data) {
          var campground_count = 0;
          for (var i = 0; i < result.data.length; i++) {
            if (result.data[i].states === key) {
              campground_count += result.data[i].campgrounds
            }
          }
          if (campground_count > 0) {
            this.data[key][1] = campground_count
            this.points.push([this.data[key][0], campground_count])
            this.states.push(key);
          }
        }
        this.setState({
          didLoad: true
        });
      })
    );
  }

  drawChart() {
    function mouseOver(stateAbbrev) {
        console.log("State abbrev", stateAbbrev);
        d3.select(this)
          .attr("fill-opacity", "0.5");

        d3.select("#tooltip")
          .html("<span>State: " + stateAbbrev + "</span")
          .style("position", "absolute")
          .style("left", (d3.event.pageX - 50) + "px")
          .style("top", (d3.event.pageY - 50) + "px")
          .style("background", "#fff")
          .style("padding", "5px")
          .style("border-radius", "5px")
          .style("opacity", .9)
          .transition()
          .duration(200);
    }

    function mouseOut() {
      d3.select(this)
        .attr("fill-opacity", "1");

      d3.select("#tooltip")
        .transition()
        .duration(200)
        .style("opacity", 0);
    }

      //Width and height
      var width = 960;
      var height = 700;
      var padding = 70;

      const margin = 75;
      // width = width - margin * 2
      // height = height - margin * 2

      //Create scale functions
      var xScale = d3.scaleLinear()
                  .domain([0, d3.max(this.points, function(d) { return d[0]; })])
                  .range([padding, width - padding * 2]);

      var yScale = d3.scaleLinear()
                  .domain([0, d3.max(this.points, function(d) { return d[1]; })])
                  .range([height - padding, padding]);

      //Define X axis
      var xAxis = d3.axisBottom(xScale)
                  .ticks(4)

      //Define Y axis
      var yAxis = d3.axisLeft(yScale)

      //Create SVG element
      var svg = d3.select("#scatter-plot")
            .append("svg")
            .attr("width", width)
            .attr("height", height)
            .attr('transform', `translate(${margin}, ${margin})`);

      svg.selectAll("circle")
         .data(this.points)
         .enter()
         .append("circle")
         .attr("cx", function(d) {
            return xScale(d[0]);
         })
         .attr("cy", function(d) {
            return yScale(d[1]);
         })
         .attr("r", 6)
         .style("fill", '#7fa46a')

      svg.selectAll("circle")
         .data(this.states)
         .on("mouseover", mouseOver)
         .on("mouseout", mouseOut);

      //Create X axis
      svg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + (height - padding) + ")")
        .call(xAxis);
      
      //Create Y axis
      svg.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(" + padding + ",0)")
        .call(yAxis);

      svg.append('text')
        .attr('x', -(height / 2))
        .attr('y', margin / 5)
        .attr('transform', 'rotate(-90)')
        .attr('text-anchor', 'middle')
        .text('Number of Campgrounds');

      svg.append('text')
        .attr('x', width / 2)
        .attr('y', height - margin / 3)
        .attr('text-anchor', 'middle')
        .text('Population Density');

      d3.select("#scatter-plot")
        .attr("width", "100%")
        .attr("display", "block");
        // .style("border", "1px solid black");
  }
        
  render() {
    return (
        <div>
            <div id="tooltip" width="100" height="100"></div>
            <svg id="scatter-plot" width="960" height="700" viewBox="0 0 960 700" preserveAspectRatio="xMidYMid meet"></svg>
        </div>
    );
  }
}
    
export default CampgroundsScatterPlot;