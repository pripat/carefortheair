import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

const styles = theme => ({

  formControl: {
    margin: theme.spacing.unit,
    minWidth: 175,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

class FilterBar extends React.Component {
  state = {
    filterValue: ""
  }

  constructor(props) {
      super(props)
      this.name = null
      this.menu = {}
      this.filterBy = null
      this.handleChange = this.handleChange.bind(this)
  }


  handleChange = event => {
    this.setState({ filterValue: event.target.value}, ()=> {
       this.props.onFilter(this.state.filterValue, this.props.filterBy)
    })
  };

  render() {
    const { classes } = this.props;
    const menuItems = this.props.menu;
    return (
      <form autoComplete="off">
        <FormControl className={classes.formControl} name='filter-menu'>
          <InputLabel htmlFor="bill-filter">{this.props.name}</InputLabel>
            <Select
              value={this.state.filterValue}
              onChange={this.handleChange}
            >
            <MenuItem value=""><em>None</em></MenuItem>
            {
              menuItems.map(value =>
                <MenuItem key={value} value={value} name='filter-item'>{value}</MenuItem>
              )
            }
            
          </Select>
        </FormControl>
      </form>
    );
  }
}

FilterBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FilterBar);