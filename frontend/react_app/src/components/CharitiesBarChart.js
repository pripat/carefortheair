import React, {Component} from 'react';
import axios from 'axios';
import * as d3 from "d3";
import d3Tip from "d3-tip"
import './tip.css'

class CharitiesBarChart extends Component {
  constructor(props) {
    super(props);
    this.data = {1:[0,[]], 2:[0,[]], 3:[0,[]], 4:[0,[]]};
    this.state = {
        didLoad: false
    };
  }

  componentDidMount() {
    this.getCharityInfo();
  }

  componentDidUpdate(prevProps, prevState) {
        const didLoad = this.state.didLoad;
        if (didLoad !== prevState.didLoad) {
            this.drawChart();
        }
    }

  getCharityInfo() {
    axios.all([axios.get("https://api.carefortheair.me/charities")]).then(
      axios.spread(result => {
              for (var i = 0; i < result.data.length; i++) {
                const rating = result.data[i].rating
                const income = result.data[i].income_amount
                this.data[rating][0] += 1
                this.data[rating][1].push(income)
              }
              this.setState({
                didLoad: true
              });
            })
        );
  }

  getSum(total, num) {
    return total + num;
  }

  calc() {
    var arr = []
    for (var key in this.data) {
      var val = Math.round(this.data[key][1].reduce(this.getSum) / this.data[key][0])
      var min = `$${Math.min(...this.data[key][1]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`
      var max = `$${Math.max(...this.data[key][1]).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`
      arr.push({ rating:key, value:val, count:this.data[key][0], min: min, max: max });
    }
    return arr
  }

  drawChart() {
    const data = this.calc()
    const svg = d3.select('#bar-chart');
    const margin = 90;
    const width = 1000 - 2 * margin;
    const height = 700 - 2 * margin;


    const chart = svg.append('g')
      .attr('transform', `translate(${margin}, ${margin})`);

    const xScale = d3.scaleBand()
      .range([0, width])
      .domain(data.map((s) => s.rating))
      .padding(0.4)
    
    const yScale = d3.scaleLinear()
      .range([height, 0])
      .domain([0, 25000000]);

    chart.append('g')
      .attr('transform', `translate(0, ${height})`)
      .call(d3.axisBottom(xScale));

    chart.append('g')
      .call(d3.axisLeft(yScale));

    const tip = d3Tip()

    tip
      .attr("class", "d3-tip")
      .offset([-10, 0])
      .html(d => {
        return (
          '<strong>Frequency:</strong> <span style="color:#31c5f7">' + d.count + 
          '</span> <br/> <strong>Min:</strong> <span style="color:#31c5f7">' + d.min +
          '</span> <br/> <strong>Max:</strong> <span style="color:#31c5f7">' + d.max +  '</span>'
        )
      })

    svg.call(tip)


     const barGroups = chart.selectAll()
      .data(data)
      .enter()
      .append('g')
  
    barGroups
      .append('rect')
      .attr('class', 'bar')
      .attr('x', (g) => xScale(g.rating))
      .attr('y', (g) => yScale(g.value))
      .attr('height', (g) => height - yScale(g.value))
      .attr('width', xScale.bandwidth())
      .attr("fill", "#31c5f7")
      .on('mouseenter', function (actual, i) {
        tip.show(actual, this)

        d3.selectAll('.value')
          .attr('opacity', 0)

        d3.select(this)
          .transition()
          .duration(300)
          .attr('opacity', 0.6)
          .attr('x', (a) => xScale(a.rating) - 5)
          .attr('width', xScale.bandwidth() + 10)

        barGroups.append('text')
          .attr('class', 'divergence')
          .attr('x', (a) => xScale(a.rating) + xScale.bandwidth() / 2)
          .attr('y', (a) => yScale(a.value) + 30)
          .attr('fill', 'white')
          .attr('text-anchor', 'middle')
          .text((a, idx) => {
            var divergence = (a.value - actual.value)
            
            let text = ''
            if (divergence > 0) text += '+'
            else {
              text += '-'
              divergence *= -1
            }
            text += `$${divergence.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`

            return idx !== i ? text : '';
          })

      })
      .on('mouseleave', function () {
        d3.selectAll('.value')
          .attr('opacity', 1)

        d3.select(this)
          .transition()
          .duration(300)
          .attr('opacity', 1)
          .attr('x', (a) => xScale(a.rating))
          .attr('width', xScale.bandwidth())

        tip.hide(this);

        chart.selectAll('#limit').remove()
        chart.selectAll('.divergence').remove()
      })

    barGroups 
      .append('text')
      .attr('class', 'value')
      .attr('x', (a) => xScale(a.rating) + xScale.bandwidth() / 2)
      .attr('y', (a) => yScale(a.value) + 30)
      .attr('text-anchor', 'middle')
      .text((a) => `$${a.value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")}`)


    svg
      .append('text')
      .attr('x', -(height / 2) - margin)
      .attr('y', margin / 5)
      .attr('transform', 'rotate(-90)')
      .attr('text-anchor', 'middle')
      .text('Income Amount')

    svg.append('text')
      .attr('x', width / 2 + margin)
      .attr('y', height + margin * 1.5)
      .attr('text-anchor', 'middle')
      .text('Charity Rating')

    svg.attr("width", "100%")
        .attr("display", "block");
        // .style("border", "1px solid black");
  }
        
  render() {
    return(
        <svg id="bar-chart" width="960" height="700" viewBox="0 0 960 700" preserveAspectRatio="xMidYMid meet"></svg>
    );
  }
}
    
export default CharitiesBarChart;