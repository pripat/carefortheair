import React from 'react';
import { Link } from 'react-router-dom';

import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const styles = {
  card: {
    maxWidth: '100%',
  },
  media: {
    objectFit: 'cover',
  },
};


const BillCard = (props) => {
	const { classes } = props;

	var enac;
	if (props.item.enacted) {
		enac = "Yes"
	} else {
    	enac = "No"
	}


	var title;
	if (props.item.short_title === props.item.title || props.item.short_title == null) {
		title = props.item.bill_number
	} else {
    	title = props.item.short_title
	}

	return(
		<div>
		<Card className={classes.card} name="bill-card">
			<CardActionArea
				component = {Link} to = {"/Bills/" + props.item.model_id} style={{textDecoration: "none"}}>
				<CardMedia
					component = "img"
					className= {classes.media}
					height="300"
					image = {props.item.bill_image}
					title =""/>
					<CardContent>
						<Typography gutterBottom variant="h5" component="h2">
							{title}
						</Typography>
						<Typography component="p">
				            <b>Bill Type: </b> {props.item.bill_type}
				        </Typography>
				        <Typography component="p">
				            <b>Sponsoring Party: </b> {props.item.sponsor_party}
				        </Typography>
				       	<Typography component="p">
				            <b>Sponsor's State: </b> {props.item.sponsor_state}
				        </Typography>
				        <Typography component="p">
				            <b>Primary Subject: </b> {props.item.primary_subject}
				        </Typography>
				        <Typography component="p">
				            <b>Enacted: </b> {enac}
				        </Typography>
				    </CardContent>
			</CardActionArea>
		</Card>
		</div>
	)
}

BillCard.propTypes = {
	classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(BillCard);
