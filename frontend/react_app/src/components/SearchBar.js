import React, {Component} from 'react';
import Input from '@material-ui/core/Input';
import SearchIcon from '@material-ui/icons/Search';
import {Button} from '@material-ui/core';


class SearchBar extends Component {

 	state = {textValue: ""}

	constructor(props) {
	    super(props)
	    this.textEntered = this.textEntered.bind(this)
	    this.textSubmitted = this.textSubmitted.bind(this)
	}

	textEntered(e) {
		this.setState({textValue: e.target.value})
	}

	textSubmitted() {
		this.props.onInputText(this.state.textValue)
	}

	render() {

		return (
			<div>
            <Input
              className="input"
              placeholder="Search…"
              variant="outlined"
              style={{width:"25vw"}}
              onChange={this.textEntered}
              name='search-input'
            />
            <Button className="button" variant="outlined" color="default" onClick={this.textSubmitted} name='search-button'><SearchIcon /></Button>
            </div>
		);
	}

}

export default SearchBar;
