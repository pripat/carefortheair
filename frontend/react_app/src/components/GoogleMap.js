import React, { Component } from 'react';

class GoogleMap extends Component {
    render() {
        let qry =  encodeURIComponent(this.props.qry)
        if (this.props.disableURI) {
            qry = this.props.qry;
        }
        return (
            <iframe title="Map Location" height="300"
            frameBorder="0" style={ {"border": 0, "width": "100%"} }
            src={"https://maps.google.com/maps/embed/v1/place?q=" +
                 qry +
                 "&key=AIzaSyD-i6i5f06RQ8422a6lB2WVu0bnL_PJxz8"}>
           </iframe>
       );
    }
}

export default GoogleMap;
