import React from 'react';
import { Link } from 'react-router-dom';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

class Navigation extends React.Component {
    constructor(props) {
      super(props);

      this.state = {
        value: 0,
      };
    }

    handleChange = (event, value) => {
      this.setState({ value });
    };

    render() {
        return (
            <div>
                <AppBar position='static' style={{background:'#BBDEFB'}}>
                  <Toolbar>
                    <Grid
                      justify="space-evenly"
                      container
                      spacing={24}
                    >
                      <Grid item>
                        <Tabs value={this.state.value} indicatorColor="primary" textColor="primary" onChange={this.handleChange}>
                          <Tab className="tab" id='HomeNavButton' component={Link} to ="/Home" color="inherit" label="Home" name='home-nav' style={{textDecoration: "none"}}/>
                          <Tab id='locationNavButton' component={Link} to ="/Locations" color="inherit" label="Locations" name='locations-nav' style={{textDecoration: "none"}}/>
                          <Tab id='charityNavButton' component={Link} to ="/Charities" color="inherit" label="Charities" name='charities-nav' style={{textDecoration: "none"}}/>
                          <Tab id='billNavButton' component={Link} to ="/Bills" color="inherit" label="Bills" name='bills-nav' style={{textDecoration: "none"}}/>
                          <Tab id='searchNavButton' component={Link} to ="/Search" color="inherit" label="Search" name='search-nav' style={{textDecoration: "none"}}/>
                          <Tab id='visualNavButton' component={Link} to ="/Visualizations" color="inherit" label="Visualizations" name='visual-nav' style={{textDecoration: "none"}}/>
                          <Tab id='aboutNavButton' component={Link} to ="/About" color="inherit" label="About" name='about-nav' style={{textDecoration: "none"}}/>
                        </Tabs>
                      </Grid>
                    </Grid>
                  </Toolbar>
                </AppBar>
              </div>
          );
      }
}

export default Navigation;
