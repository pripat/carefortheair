import React from 'react';
import CharityCard from '../components/CharityCard.js';
import ContextCard from '../components/ContextCard.js';
import Grid from '@material-ui/core/Grid';
import Pagination from '../components/Pagination.js';
import axios from 'axios';
import charitybanner from '../assets/charitiesbanner.png';
import {Container, Image, Col, Row} from 'react-bootstrap';
import SearchBar from '../components/SearchBar.js';
import FilterBar from '../components/FilterBar.js';

const state = ['Alabama','Alaska','Arizona','Arkansas','California','Colorado',
 'Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois',
 'Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland',
 'Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana',
 'Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York',
 'North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania',
 'Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah',
 'Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming']

const classification = [
    'Wildlife Preservation, Protection',
    'Alliance/Advocacy Organizations ',
    'Forest Conservation',
    'Recreational and Sporting Camps',
    'International Relief',
    'Fund Raising and/or Fund Distribution ',
    'Research Institutes and/or Public Policy Analysis ',
    'Zoo, Zoological Society',
    'Education N.E.C.',
    'Natural Resources Conservation and Protection',
    'Land Resources Conservation',
    'Consumer Protection, Safety',
    'Pollution Abatement and Control Services',
    'Water Resource, Wetlands Conservation and Management',
    'Public Interest Law, Litigation',
    'Other']

const rating = [
    '1',
    '2',
    '3',
    '4'
]

const sort = [
    'Name (ascending)',
    'Name (descending)',
    'Rating (ascending)',
    'Rating (descending)'
]


class Charities extends React.Component {
    constructor(props) {
        super(props)
        this.url = "https://api.carefortheair.me/charities/search?"
        this.text = null
        this.filterState = ""
        this.filterCategory = ""
        this.filterRating = ""
        this.sort = ""
        this.sortDesc = null
        this.handleSearchInput = this.handleSearchInput.bind(this)
        this.handleFilter = this.handleFilter.bind(this)
        this.state = {
            charities: [], currentCharities: [], currentPage: null, totalPages: null,
            url: "https://api.carefortheair.me/charities/search?", charityQuery: ""
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const {url} = this.state
        if (url !== prevState.url) {
            this.getInfo();
        }
    }

    getInfo() {
        axios.all([axios.get(this.state.url)]).then(
          axios.spread(result => {
            this.setState({
              charities: result.data.results,
              charityQuery: result.data.query
            }, () => {this.onPageChanged({
                currentPage: 1,
                totalPages: Math.ceil(result.data.results.length / 15),
                pageLimit: 15,
                totalRecords: result.data.results.length
            })});
          })
        );
    }

        handleSearchInput(text) {
        if (null !== text) {
            this.text = text.replace(" ", "%20")

            if (this.text !== "") {
                if (this.state.charityQuery === ""){
                    this.url = this.url.replace("search?", "search?query=" + text)
                }
                else{
                    this.url = this.url.replace("query=" + this.state.charityQuery, "query=" + text)
                }
            }
            else {
                this.url = this.url.replace("query=" + this.state.charityQuery, '')
            }
            this.setState({url: this.url})
        }
    }

    handleFilter(text, attr) {
        text = text.replace(/ /g, "%20")
        var val = "&" + attr + "=" + text
        if (text === "") {
            val = ""
        }

        if (attr === "state") {
            if (this.filterState === "") {
                this.url += val
            } else {
                const oldVal = "&" + attr + "=" + this.filterState
                this.url = this.url.replace(oldVal, val)
            }
            this.filterState = text
        } else if (attr === "classification") {
            if (this.filterCategory === "") {
                this.url += val
            } else {
                const oldVal = "&" + attr + "=" + this.filterCategory
                this.url = this.url.replace(oldVal, val)
            }
            this.filterCategory = text
        } else if (attr === "rating") {
            if (this.filterRating === "") {
                this.url += val
            } else {
                const oldVal = "&" + attr + "=" + this.filterRating
                this.url = this.url.replace(oldVal, val)
            }
            this.filterRating = text
        } else {
            var queryBool = true
            if (text.includes("ascending")) {
                queryBool = false
            }
            if (text.includes("Name")) {
                text = 'Name'
            } else {
                text = 'Rating'
            }
            val = "&" + attr + "=" + text + "&desc=" + queryBool
            if (this.sort === "") {
                this.url += val
            } else {
                const oldVal = "&" + attr + "=" + this.sort + "&desc=" + this.sortDesc
                this.url = this.url.replace(oldVal, val)
            }
            this.sort = text
            this.sortDesc = queryBool
        }

        this.setState({url: this.url})
    }

    componentDidMount() {
        this.getInfo();
    }

    onPageChanged = data => {
        var { charities } = this.state;
        const { currentPage, totalPages, pageLimit } = data;

        const offset = (currentPage - 1) * pageLimit;
        const currentCharities = charities.slice(offset, offset + pageLimit);

        this.setState({ currentPage, currentCharities, totalPages });
    };

    render() {
        const classes = this.props;
        var {charities, currentCharities, charityQuery} = this.state
        const totalCharities = charities.length
        console.log("url",  this.url)
        return (
        	<React.Fragment>
            <div style={{"paddingTop":50}}>
            <Container >
                <Row>
                <Col>
                <div  align="center">
                    <Image src = {charitybanner} fluid/>
                    <div style={{padding:20}} align="center" >
                        <SearchBar link="/Charity/search" onInputText={this.handleSearchInput}/>
                        <b> </b> <br />
                        <Grid container spacing ={32} justify="center" direction="row">
                            <FilterBar className="filtering" name="Filter by state" menu={state} filterBy="state" onFilter={this.handleFilter} />
                            <FilterBar name="Filter by classification" menu={classification} filterBy="classification" onFilter={this.handleFilter} />
                            <FilterBar name="Filter by rating" menu={rating} filterBy="rating" onFilter={this.handleFilter} />
                            <FilterBar className="sorting" name="Sort by" menu={sort} filterBy="sort" onFilter={this.handleFilter} />
                        </Grid>
                    </div>
                </div>
                </Col>
                </Row>
            </Container>
            </div>
            <Container>
                <div style={{ padding: 30 }}>
                    <Grid container spacing={24} justify="center" >
                    	{currentCharities.map (item =>
                            (item.search_context === undefined || !this.url.includes("query=") ?
                            <Grid item xs={12} sm={4} key={item.model_id}>
                                <CharityCard
                                    item={item}
                                    key={item.model_id}
                                />
                            </Grid>:
                            <Grid item xs={12} key={item.model_id} name>
                                    <ContextCard
                                        query={charityQuery}
                                        item={item}
                                        key={item.model_id}
                                        image={item.charity_photo}
                                        extension="/Charities/"
                                        cardName="charity-context-card"
                                        title={item.name}
                                    />

                            </Grid>))}
                    </Grid>
                    <br />
                    {totalCharities !== 0 && (
                      <Pagination
                        className={classes.pagination}
                        currentPage={this.state.currentPage}
                        totalRecords={totalCharities}
                        pageLimit={15}
                        onPageChanged={this.onPageChanged}
                      />
                    )}
                </div>
            </Container>
            </React.Fragment>
        );
    }
}

export default Charities;
