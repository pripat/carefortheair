import React, { Component } from "react";
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AQIMap from "../components/AQIMap.js";
import CharitiesBarChart from "../components/CharitiesBarChart.js";
import BillsSunburst from "../components/BillsSunburst.js";
import DensityMap from "../components/DensityMap.js";
import CampgroundsScatterPlot from "../components/CampgroundsScatterPlot.js";
import FeeGraph from "../components/FeeGraph.js";
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Grid from '@material-ui/core/Grid';
import {Image} from 'react-bootstrap';
import visualbanner from '../assets/visualbanner.png';

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
});

class Visualizations extends Component {
    constructor(props) {
    super(props);
        this.state = {value: 0};
    }

    handleChange = (event, value) => {
    this.setState({ value });
    };

    render() {
        const { value } = this.state
		return(
			<div>
                <div align="center" style={{"paddingTop":50, "paddingBottom": 50}}>
                    <Image  src = {visualbanner}  fluid/>
                </div>
                <AppBar position="static" style={{background:'#EEEEEE'}} >
                <Grid
                  justify="space-evenly"
                  container
                  spacing={40}
                  alignItems="center"
                >
                  <Grid item>
                  <Tabs value={value} indicatorColor="primary" textColor="primary" onChange={this.handleChange}>
                    <Tab label="AQI Map" name='aqi-map-tab'/>
                    <Tab label="Charity Barchart" name='barchart-tab'/>
                    <Tab label="Bill Sunburst" name='sunburst-tab'/>
                    <Tab label="Provider Visualizations" name='density-map-tab'/>
                  </Tabs>
                  </Grid>
                  </Grid>
                </AppBar>
                <br />
                {value === 0 &&
                  <div>
                    <Typography align="center" variant="h3">
                        Average AQI for the States
                    </Typography>
                    <AQIMap/>
                  </div>
                }
                {value === 1 &&
                  <div>
                    <Typography align="center" variant="h3">
                        Income Amount and Rating for Charities
                    </Typography>
                    <CharitiesBarChart/>
                  </div>
                }
                {value === 2 &&
                  <div>
                    <Typography align="center"  variant="h3">
                        Categories for Bills
                    </Typography>
                    <br />
                    <BillsSunburst/>
                  </div>
                }
                {value === 3 &&
                  <div>
                    <Typography align="center"  variant="h3">
                        State Density Rankings
                    </Typography>
                    <br />
                    <DensityMap/>
                    <br />
                    <Typography align="center"  variant="h3">
                        Campgrounds Scatter Plot
                    </Typography>
                    <CampgroundsScatterPlot/>
                    <br />
                    <Typography align="center"  variant="h3">
                        Average Fee per Park Area Graph
                    </Typography>
                    <FeeGraph/>
                  </div>
                }
                <footer className="page-footer font-small blue pt-4">
                    <div className="footer-copyright text-center py-3">
                        © 2019 Copyright: Clouds. wow.
                    </div>
                </footer>
			</div>
		);
	}
}
function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

export default  withStyles(styles)(Visualizations);
