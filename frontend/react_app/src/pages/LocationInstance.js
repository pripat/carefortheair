import React, { Component } from "react";
import axios from 'axios';
import GoogleMap from '../components/GoogleMap.js';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import YouTube from 'react-youtube'

import CharityCard from '../components/CharityCard.js';
import BillCard from '../components/BillCard.js'

const Header = {
  color: "black",
  fontSize: "40px",
  textAlign: "center"
};

class LocationInstance extends Component {
  constructor(props) {
    super(props);

    this.state = {
      locationID: this.props.match.params.id,
      attr: null,
      didLoad: false,
      relatedCharity: null,
      relatedBill: null
    }
  }

  async componentDidMount() {
    const getLocation = await axios.get('https://d3vnch75mm4o9r.cloudfront.net/locations/' + this.state.locationID);
    const getCharity = await axios.get('https://d3vnch75mm4o9r.cloudfront.net/charities/' + getLocation.data[0].related_charity_id);
    const getBill = await axios.get('https://d3vnch75mm4o9r.cloudfront.net/bills/' + getLocation.data[0].related_bill_id);

    this.setState({
      attr: getLocation.data[0],
      didLoad: true,
      relatedCharity: getCharity.data[0],
      relatedBill: getBill.data[0]
    });
  }

  render() {
    if (this.state.didLoad) {
      const attr = this.state.attr;
      const display = attr.wiki_image;
      const city = attr.city_name;
      const youtube = attr.youtube_video_id;
      const relatedCharity = this.state.relatedCharity;
      const relatedBill = this.state.relatedBill;
      const locQry = [].concat([attr.latitude, attr.longitude]).join("+");
      const image = (display) ? (display) : "https://www.metoffice.gov.uk/binaries/content/gallery/metofficegovuk/hero-images/weather/cloud/cumulonimbus-2.jpg/cumulonimbus-2.jpg/metofficegovuk%3AheroMedium";

      const opts = {
      height: '300',
      width: '410',
      playerVars: {}};

      return (
        <div>
            <br />
            <h1 style={Header} name="title"> {city} </h1>
            <div>
              <br />
              <Grid container direction="row" justify="center" alignItems="center" spacing={24}>
                  <Grid item xs={8} sm={6}>
                      <div align="center">
                          <img src={image} style={{"width":"100%"}} border="0" alt="Null"/>
                          <h1> </h1> <br />
                      </div>
                            <Typography >
                                <b> Description: </b> {attr.wiki_text} <br />
                            </Typography>
                            <Typography>
                                <b> Air Quality Index: </b> {attr.aqi} <br />
                            </Typography>
                            <Typography>
                                <b> Dominant Pollutant: </b> {attr.dominant_pol} <br />
                            </Typography>
                            <Typography>
                                <b> Air Quality URL: </b> <a href={attr.aqicn_url}> {city} Air Quality </a> <br />
                            </Typography>
                      <br />
                  </Grid>
                  <br />
                  <Grid item xs={8} sm={4}>
                      <div>
                          <figure className="figure" style={ {"width": "100%"} }>
                             <GoogleMap qry={locQry} disableURI/>
                          </figure>
                          <h1> </h1> <br />
                          {youtube == null ?
                            (<YouTube videoId={"e6rglsLy1Ys"} opts={opts} onReady={this._onReady}/>) :
                            (<YouTube videoId={youtube} opts={opts} onReady={this._onReady}/>)}
                      </div>
                  </Grid>
             </Grid>
             <h1> </h1> <br />
             <h1> </h1> <br />
            </div>
         <div>
              <Grid container direction="row" justify="center" alignItems="baseline" spacing={24}>
                    <Grid item xs={8} sm={4} name='related-charity'>
                        <Typography>
                            <b> Related Charity </b>
                        </Typography>
                             <CharityCard item={relatedCharity} />
                    </Grid>
                     <Grid item xs={8} sm={4} name='related-bill'>
                         <Typography>
                             <b> Related Bill </b>
                         </Typography>
                            <BillCard item={relatedBill} />
                        </Grid>
                </Grid>
          </div>
            <footer className="page-footer font-small blue pt-4">
                <div className="footer-copyright text-center py-3"> © 2019 Copyright: Clouds. wow. </div>
            </footer>
    </div>
      );
    } else {
      return(<div></div>);
    }

  }
  _onReady(event) {
      // access to player in all event handlers via event.target
      event.target.pauseVideo();
  }
}

export default LocationInstance;
