import React, { Component } from "react";
import axios from 'axios';
import { TwitterTimelineEmbed } from 'react-twitter-embed';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import LocationCard from '../components/LocationCard.js';
import CharityCard from '../components/CharityCard.js'

const Header = {
  color: "black",
  fontSize: "40px",
  textAlign: "center"
};

class BillInstance extends Component {
  constructor(props) {
    super(props);

    this.state = {
      billsID: this.props.match.params.id,
      attr: null,
      didLoad: false,
      relatedLocation: null,
      relatedCharity:  null
    }
  }

  async componentDidMount() {
    const getBill = await axios.get('https://d3vnch75mm4o9r.cloudfront.net/bills/' + this.state.billsID);
    const getLocation = await axios.get('https://d3vnch75mm4o9r.cloudfront.net/locations/' + getBill.data[0].related_location_id);
    const getCharity = await axios.get('https://d3vnch75mm4o9r.cloudfront.net/charities/' + getBill.data[0].related_charity_id);

    this.setState({
      attr: getBill.data[0],
      didLoad: true,
      relatedLocation: getLocation.data[0],
      relatedCharity: getCharity.data[0]
    });
  }

  render() {
    if (this.state.didLoad) {
      const attr = this.state.attr;
      const billInfo = attr.title;
      const sponsorimg = attr.sponsor_image;
      const sponsorTitle = attr.sponsor_title;
      const sponsorName = attr.sponsor_name;
      const sponsorParty = attr.sponsor_party;
      const primSub = attr.primary_subject;
      const billpdf = attr.bill_pdf;
      const latestAction = attr.latest_action;
      const twitter = attr.sponsor_twitter;
      const relatedLocation = this.state.relatedLocation;
      const relatedCharity = this.state.relatedCharity;

      var title;
      if (attr.short_title === attr.title || attr.short_title == null) {
        title = attr.bill_number;
      } else {
        title = attr.short_title;
      }

      var committee;
      if (attr.committees === null) {
        committee = "N/A";
      } else {
        committee = attr.committees;
      }

      return (
        <div>
            <br />
            <h1 style={Header} name="title"> {title} </h1>
            <div className="Content" style={{margin:45, paddingLeft:80}}>
                <br />
                <Grid container direction="row" justify="space-around" alignItems="center" spacing={12}>
                    <Grid item xs={8}>
                        <Card className="Content">
                          <CardContent>
                              <Typography>
                                  <b> Title: </b> {billInfo} <br />
                              </Typography>
                              <Typography>
                                  <b> Primary Subject: </b> {primSub} <br />
                              </Typography>
                              <Typography>
                                  <b> Sponsor: </b> {sponsorTitle} {sponsorName} ({sponsorParty}) <br />
                              </Typography>
                              <Typography>
                                  <b> Last Major Action: </b> {latestAction} <br />
                              </Typography>
                              <Typography>
                                  <b> Committee: </b> {committee} <br />
                              </Typography>
                          </CardContent>
                        </Card>
                        <br />
                        <Card className="Content">
                            <CardContent>
                              <Typography align="center">
                                  { billpdf=== null ? (<b>BILL NOT AVAILABLE</b>) : (<embed src= {billpdf} width="500" height="375" type="application/pdf"/>) }
                              </Typography>
                            </CardContent>
                        </Card>
                    </Grid>
                    <Grid item xs={4}>
                        {twitter == null ?
                            (<Typography align="center">
                            <img src={sponsorimg} alt=" " width="350vw"/> <br />
                            <b> {"Sponsor: " + sponsorName} </b> </Typography>) :
                            (<Typography align="center">
                            <b> {"Sponsor: " + sponsorName} </b><br />
                            <img src={sponsorimg} alt=" " width="250"/> <br />
                            <b> </b><br />
                            <TwitterTimelineEmbed sourceType="profile" screenName={twitter} options={{height: 300, width: 250}}/></Typography>)}
                    </Grid>
            </Grid>
            <h1> </h1> <br />
            <h1> </h1> <br />
        </div>
        <div>
            <Grid container direction="row" justify="center" alignItems="baseline" spacing={24}>
                  <Grid item xs={8} sm={4} name='related-charity'>
                      <Typography>
                          <b> Related Charity </b>
                      </Typography>
                           <CharityCard item={relatedCharity} />
                  </Grid>
                   <Grid item xs={8} sm={4} name='related-location'>
                       <Typography>
                           <b> Affected Location </b>
                       </Typography>
                          <LocationCard item={relatedLocation} />
                  </Grid>
            </Grid>
        </div>
        <footer className="page-footer font-small blue pt-4">
            <div className="footer-copyright text-center py-3">
                © 2019 Copyright: Clouds. wow.
            </div>
        </footer>
     </div>
      );
    } else {
      return(<div></div>);
    }
  }
}

export default BillInstance;
