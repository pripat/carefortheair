import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import SearchBar from '../components/SearchBar.js';
import ContextCard from '../components/ContextCard.js';
import {Container, Col, Row} from 'react-bootstrap';
import axios from 'axios';
import Grid from '@material-ui/core/Grid';
import Pagination from '../components/Pagination.js';


const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
});

class Search extends React.Component {
  constructor(props) {
    super(props);
    this.text = null;
    this.url = "https://api.carefortheair.me/all/search?"
    this.handleSearchInput = this.handleSearchInput.bind(this)
    this.state = {
      locations: [], currentLocations: [], currentLocationPage: null, totalLocationPages: null,
      charities: [], currentCharities: [], currentCharityPage: null, totalCharityPages: null,
      bills: [], currentBills: [], currentBillPage: null, totalBillPages: null,
      value: 0, query: "",
      url: "https://api.carefortheair.me/all/search?query="
    };
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleSearchInput(text) {
    if (text !== null) {
      this.text = text.replace(" ", "%20")
      this.url = "https://api.carefortheair.me/all/search?query="
      if (this.text !== "") {
        this.url += this.text
      }
      this.setState({url: this.url})
    }
  }

  componentDidMount() {
    this.getInfo();
  }

  componentDidUpdate(prevProps, prevState) {
    const {url} = this.state
    if (url !== prevState.url) {
      this.getInfo();
    }
  }

  getInfo() {
    axios.all([axios.get(this.state.url)]).then(
      axios.spread(result => {
        this.setState({
          locations: result.data.locations.results,
          charities: result.data.charities.results,
          bills: result.data.bills.results,
          query: result.data.locations.query
        }, () => {
          this.onLocationPageChanged({
            currentPage: 1,
            totalPages: Math.ceil(result.data.locations.results.length / 15),
            pageLimit: 15,
            totalRecords: result.data.locations.results.length
          });
          this.onCharityPageChanged({
            currentPage: 1,
            totalPages: Math.ceil(result.data.charities.results.length / 15),
            pageLimit: 15,
            totalRecords: result.data.charities.results.length
          });
          this.onBillPageChanged({
            currentPage: 1,
            totalPages: Math.ceil(result.data.bills.results.length / 15),
            pageLimit: 15,
            totalRecords: result.data.bills.results.length
          });
        });
      })
    );
  }

  onLocationPageChanged = data => {
    var { locations } = this.state;
    const { currentPage, totalPages, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const currentLocations = locations.slice(offset, offset + pageLimit);

    this.setState({ currentLocationPage: currentPage, currentLocations, totalLocationPages: totalPages });
  };

  onCharityPageChanged = data => {
    var { charities } = this.state;
    const { currentPage, totalPages, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const currentCharities = charities.slice(offset, offset + pageLimit);

    this.setState({ currentCharityPage: currentPage, currentCharities, totalCharityPages: totalPages });
  };

  onBillPageChanged = data => {
    var { bills } = this.state;
    const { currentPage, totalPages, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const currentBills = bills.slice(offset, offset + pageLimit);

    this.setState({ currentBillPage: currentPage, currentBills, totalBillPages: totalPages });
  };

  render() {
    const { classes } = this.props;
    const { locations, currentLocations, charities, currentCharities, bills, currentBills, value, query } = this.state;
    const totalLocations = locations.length;
    const totalCharities = charities.length;
    const totalBills = bills.length;

    return (
      <div className={classes.root} style={{background:'#F5F5F5'}}>
        <Container >
            <Row>
            <Col>
            <div align="center">
                <div style={{padding:20}} align="center">
                    <SearchBar link="/Search" onInputText={this.handleSearchInput}/>
                </div>
            </div>
            </Col>
            </Row>
        </Container>
        <br/>
        <AppBar position="static" style={{background:'#BBDEFB'}}>
        <Grid
          justify="space-evenly"
          container
          spacing={40}
          alignItems="center"
        >
          <Grid item>
          <Tabs value={value} indicatorColor="primary" textColor="primary" onChange={this.handleChange}>
            <Tab label="Locations" name='locations-tab'/>
            <Tab label="Charities" name='charities-tab'/>
            <Tab label="Bills" name='bills-tab'/>
          </Tabs>
          </Grid>
          </Grid>
        </AppBar>

        {value === 0 &&
          <Container>
          <div style={{padding: 30}}>
          <Grid container spacing={24} justify="center">
            {currentLocations.map (item =>
              <Grid item xs={12} key={item.model_id}>
                <ContextCard query={query} item={item} key={item.model_id} image={item.wiki_image} extension="/Locations/" cardName="location-context-card"
                            title={item.city_name}/>
              </Grid>)
            }
          </Grid>
          </div>
          </Container>
        }

        {value === 0 && totalLocations !== 0 &&
          <Pagination
            className={classes.pagination}
            currentPage={this.state.currentLocationPage}
            totalRecords={totalLocations}
            pageLimit={15}
            onPageChanged={this.onLocationPageChanged}
          />
        }

        {value === 1 &&
          <Container>
          <div style={{padding: 30}}>
          <Grid container spacing={24} justify="center">
            {currentCharities.map (item =>
              <Grid item xs={12} key={item.model_id}>
                <ContextCard query={query} item={item} key={item.model_id} image={item.charity_photo} extension="/Charities/" cardName="charity-context-card"
                            title={item.name}/>
              </Grid>)
            }
          </Grid>
          </div>
          </Container>
        }

        {value === 1 && totalCharities !== 0 &&
          <Pagination
            className={classes.pagination}
            currentPage={this.state.currentCharityPage}
            totalRecords={totalCharities}
            pageLimit={15}
            onPageChanged={this.onCharityPageChanged}
          />
        }

        {value === 2 &&
          <Container>
          <div style={{padding: 30}}>
          <Grid container spacing={24} justify="center">
            {currentBills.map (item =>
              <Grid item xs={12}key={item.model_id}>
                <ContextCard query={query} item={item} key={item.model_id} image={item.bill_image} extension="/Bills/" cardName="bill-context-card"
                            title={item.short_title}/>
              </Grid>)
            }
          </Grid>
          </div>
          </Container>
        }

        {value === 2 && totalBills !== 0 &&
          <Pagination
            className={classes.pagination}
            currentPage={this.state.currentBillPage}
            totalRecords={totalBills}
            pageLimit={15}
            onPageChanged={this.onBillPageChanged}
          />
        }

      </div>
    );
  }
}

function TabContainer(props) {
  return (
    <Typography component="div" style={{ padding: 8 * 3 }}>
      {props.children}
    </Typography>
  );
}

Search.propTypes = {
  classes: PropTypes.object.isRequired,
};

TabContainer.propTypes = {
  children: PropTypes.node.isRequired,
};

export default withStyles(styles)(Search);
