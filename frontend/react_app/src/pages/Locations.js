import React from 'react';
import LocationCard from '../components/LocationCard.js';
import ContextCard from '../components/ContextCard.js';
import Grid from '@material-ui/core/Grid';
import Pagination from '../components/Pagination.js';
import axios from 'axios';
import locationbanner from '../assets/locationsbanner.png';
import {Container, Image, Col, Row} from 'react-bootstrap';
import SearchBar from '../components/SearchBar.js';
import FilterBar from '../components/FilterBar.js';

const state = ['Alabama','Alaska','Arizona','Arkansas','California','Colorado',
 'Connecticut','Delaware','Florida','Georgia','Hawaii','Idaho','Illinois',
 'Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Maryland',
 'Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana',
 'Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York',
 'North Carolina','North Dakota','Ohio','Oklahoma','Oregon','Pennsylvania',
 'Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah',
 'Vermont','Virginia','Washington','West Virginia','Wisconsin','Wyoming']

const dominant_pol = [
    'O3',
    'pm10',
    'pm25'
]

const air_status = [
    'good',
    'moderate',
    'unhealthy for sensitive groups',
    'unhealthy',
    'very unhealthy',
    'hazardous'
]

const sort = [
    'Name (ascending)',
    'Name (descending)',
    'AQI (ascending)',
    'AQI (descending)',
    'Population (ascending)',
    'Population (descending)'
]

class Locations extends React.Component {
    constructor(props) {
        super(props);
        this.url = "https://api.carefortheair.me/locations/search?"
        this.text = null
        this.filterState = ""
        this.filterDominantPol = ""
        this.filterAirStatus = ""
        this.sort = ""
        this.sortDesc = null
        this.handleSearchInput = this.handleSearchInput.bind(this)
        this.handleFilter = this.handleFilter.bind(this)
        this.state = {
            locations: [], currentLocations: [], currentPage: null, totalPages: null,
            url: "https://api.carefortheair.me/locations/search?", locationQuery: ""
        }
    }

    componentDidUpdate(prevProps, prevState) {
        const {url} = this.state
        if (url !== prevState.url) {
            this.getInfo();
        }
    }

    getInfo() {
        axios.all([axios.get(this.state.url)]).then(
          axios.spread(result => {
            this.setState({
              locations: result.data.results,
              locationQuery: result.data.query
            }, () => {this.onPageChanged({
                currentPage: 1,
                totalPages: Math.ceil(result.data.results.length / 15),
                pageLimit: 15,
                totalRecords: result.data.results.length
            })});
          })
        );
    }

    handleSearchInput(text) {
        if (null !== text) {
            this.text = text.replace(" ", "%20")

            if (this.text !== "") {
                if (this.state.locationQuery === ""){
                    this.url = this.url.replace("search?", "search?query=" + text)
                }
                else{
                    this.url = this.url.replace("query=" + this.state.locationQuery, "query=" + text)
                }
            }
            else {
                this.url = this.url.replace("query=" + this.state.locationQuery, '')
            }
            this.setState({url: this.url})
        }
    }

    handleFilter(text, attr) {
        text = text.replace(/ /g, "%20")
        var val = "&" + attr + "=" + text
        if (text === "") {
            val = ""
        }

        if (attr === "state") {
            if (this.filterState === "") {
                this.url += val
            } else {
                const oldVal = "&" + attr + "=" + this.filterState
                this.url = this.url.replace(oldVal, val)
            }
            this.filterState = text
        } else if (attr === "dominant-pol") {
            if (this.filterDominantPol === "") {
                this.url += val
            } else {
                const oldVal = "&" + attr + "=" + this.filterDominantPol
                this.url = this.url.replace(oldVal, val)
            }
            this.filterDominantPol = text
        } else if (attr === "air-status") {
            if (this.filterAirStatus === "") {
                this.url += val
            } else {
                const oldVal = "&" + attr + "=" + this.filterAirStatus
                this.url = this.url.replace(oldVal, val)
            }
            this.filterAirStatus = text
        } else {
            var queryBool = true
            if (text.includes("ascending")) {
                queryBool = false
            }
            if (text.includes("Name")) {
                text = "Name"
            } else if (text.includes("AQI")) {
                text = "AQI"
            } else {
                text = "Population"
            }
            val = "&" + attr + "=" + text + "&desc=" + queryBool
            if (this.sort === "") {
                this.url += val
            } else {
                const oldVal = "&" + attr + "=" + this.sort + "&desc=" + this.sortDesc
                this.url = this.url.replace(oldVal, val)
            }
            this.sort = text
            this.sortDesc = queryBool
        }

        this.setState({url: this.url})
    }

    componentDidMount() {
        this.getInfo();
    }

    onPageChanged = data => {
        var { locations } = this.state;
        const { currentPage, totalPages, pageLimit } = data;

        const offset = (currentPage - 1) * pageLimit;
        const currentLocations = locations.slice(offset, offset + pageLimit);

        this.setState({ currentPage, currentLocations, totalPages });
    };

    render() {
        const classes = this.props;
        var {locations, currentLocations, locationQuery} = this.state;
        const totalLocations = locations.length;
        console.log("url",  this.url)
        return (
        	<React.Fragment>
            <div style={{"paddingTop":50}}>
            <Container >
                <Row>
                <Col>
                <div  align="center">
                    <Image src = {locationbanner} fluid/>
                    <div style={{ padding:20 }} align="center" >
                        <SearchBar link="/Location/Search" onInputText={this.handleSearchInput}/>
                        <b> </b> <br />
                        <Grid container spacing ={32} justify="center" direction="row">
                            <FilterBar className="filtering" name="Filter by state" menu={state} filterBy="state" onFilter={this.handleFilter} />
                            <FilterBar name="Filter by pollutant" menu={dominant_pol} filterBy="dominant-pol" onFilter={this.handleFilter} />
                            <FilterBar name="Filter by air status" menu={air_status} filterBy="air-status" onFilter={this.handleFilter} />
                            <FilterBar className="sorting" name="Sort by" menu={sort} filterBy="sort" onFilter={this.handleFilter} />
                        </Grid>
                    </div>
                </div>
                </Col>
                </Row>
            </Container>
            </div>
            <Container>
                <div style={{ padding: 30 }}>
                    <Grid container spacing={24} justify="center">
                    	{currentLocations.map (item =>
                    	   (item.search_context === undefined || !this.url.includes("query=") ?
                            <Grid item xs={12} sm={4} key={item.model_id}>
                                <LocationCard
                                    item={item}
                                    key={item.model_id}
                                />
                            </Grid>:
                            <Grid item xs={12} key={item.model_id}>
                                    <ContextCard
                                        query={locationQuery}
                                        item={item}
                                        key={item.model_id}
                                        image={item.wiki_image}
                                        extension="/Locations/"
                                        cardName="location-context-card"
                                        title={item.city_name}
                                    />
                            </Grid>))}
                    </Grid>
                    <br />
                    {totalLocations !== 0 && (
                      <Pagination
                        className={classes.pagination}
                        currentPage={this.state.currentPage}
                        totalRecords={totalLocations}
                        pageLimit={15}
                        onPageChanged={this.onPageChanged}
                      />
                    )}
                </div>
            </Container>
            </React.Fragment>
        );
    }
}


export default Locations;
