import React, {Component} from 'react';
import BillCard from '../components/BillCard.js';
import Grid from '@material-ui/core/Grid';
import Pagination from '../components/Pagination.js';
import axios from 'axios';
import billbanner from '../assets/billbanner.png';
import {Container, Image, Col, Row} from 'react-bootstrap';
import SearchBar from '../components/SearchBar.js';
import FilterBar from '../components/FilterBar.js';
import ContextCard from '../components/ContextCard.js';

const types = [
    'Senate Bill',
    'House Joint Resolution',
    'Senate Joint Resolution',
    'Senate Resolution',
    'House Resolution',
    'House Bill',
    'House Concurrent Resolution'
]

const parties = [
    'Democratic',
    'Republican'
]

const subjects = [
    'Health',
    'Environmental Protection',
    'Transportation and Public Works',
    'Housing and community development',
    'Science, Technology, Communications',
    'Government Operations and Politics',
    'Public Lands and Natural Resources',
    'Economics and Public Finance',
    'Congress',
    'Taxation',
    'Energy',
    'Commerce',
    'International Affairs'
]

const sort = [
    'Title (ascending)',
    'Title (descending)',
    'Action-date (ascending)',
    'Action-date (descending)'
]

class Bills extends Component {
    constructor(props) {
        super(props)
        this.url = "https://api.carefortheair.me/bills/search?"
        this.text = null
        this.filterType = ""
        this.filterParty = ""
        this.filterSubject = ""
        this.sort = ""
        this.sortDesc = null
        this.handleSearchInput = this.handleSearchInput.bind(this)
        this.handleFilter = this.handleFilter.bind(this)
        this.state = {
            bills: [], currentBills: [], currentPage: null, totalPages: null,
            url: "https://api.carefortheair.me/bills/search?", billQuery: ""
        }
    }

    componentDidUpdate(prevProps, prevState) {
    	const {url} = this.state
    	if (url !== prevState.url) {
    		this.getInfo();
    	}
    }

    getInfo() {
        axios.all([axios.get(this.state.url)]).then(
          axios.spread(result => {
            this.setState({
              bills: result.data.results,
              billQuery: result.data.query
            }, ()=> {this.onPageChanged({
                currentPage: 1,
                totalPages: Math.ceil(result.data.results.length / 15),
                pageLimit: 15,
                totalRecords: result.data.results.length
            })});
          })
        );
    }

    handleSearchInput(text) {
        if (null !== text) {
            this.text = text.replace(" ", "%20")

            if (this.text !== "") {
                if (this.state.billQuery === ""){
                    this.url = this.url.replace("search?", "search?query=" + text)
                }
                else{
                    this.url = this.url.replace("query=" + this.state.billQuery, "query=" + text)
                }
            }
            else {
                this.url = this.url.replace("query=" + this.state.billQuery, '')
            }
            this.setState({url: this.url})
        }
    }

    handleFilter(text, attr) {
        text = text.replace(/ /g, "%20")
        var val = "&" + attr + "=" + text
        if (text === "") {
            val = ""
        }

        if (attr === "bill-type") {
            if (this.filterType === "") {
                this.url += val
            } else {
                const oldVal = "&" + attr + "=" + this.filterType
                this.url = this.url.replace(oldVal, val)
            }
            this.filterType = text
        } else if (attr === "party") {
            if (this.filterParty === "") {
                this.url += val
            } else {
                const oldVal = "&" + attr + "=" + this.filterParty
                this.url = this.url.replace(oldVal, val)
            }
            this.filterParty = text
        } else if (attr === "subject") {
            if (this.filterSubject === "") {
                this.url += val
            } else {
                const oldVal = "&" + attr + "=" + this.filterSubject
                this.url = this.url.replace(oldVal, val)
            }
            this.filterSubject = text
        } else {
            var queryBool = true
            if (text.includes("ascending")) {
                queryBool = false
            }
            if (text.includes("Title")) {
                text = 'Title'
            } else {
                text = 'Action-date'
            }
            val = "&" + attr + "=" + text + "&desc=" + queryBool
            if (this.sort === "") {
                this.url += val
            } else {
                const oldVal = "&" + attr + "=" + this.sort + "&desc=" + this.sortDesc
                this.url = this.url.replace(oldVal, val)
            }
            this.sort = text
            this.sortDesc = queryBool
        }

        this.setState({url: this.url})
    }

    componentDidMount() {
        this.getInfo();
    }

    onPageChanged = data => {
        var { bills } = this.state;
        const { currentPage, totalPages, pageLimit } = data;

        const offset = (currentPage - 1) * pageLimit;
        const currentBills = bills.slice(offset, offset + pageLimit);

        this.setState({ currentPage, currentBills, totalPages })
    };

    render() {
        const classes = this.props;
        var {bills, currentBills, billQuery} = this.state;
        const totalBills = bills.length;
        console.log("url",  this.url)
        return (
        	<React.Fragment>
            <div style={{"paddingTop":50}}>
            <Container >
                <Row>
                <Col>
                <div  align="center">
                    <Image  src = {billbanner}  fluid/>
                    <div style={{padding:20}} align="center" >
                        <SearchBar link="/Bill/search" onInputText={this.handleSearchInput}/>
                        <b> </b> <br />
                        <Grid container spacing ={24} justify="center" direction="row">
                            <FilterBar className="filtering" name="Filter by type" menu={types} filterBy="bill-type" onFilter={this.handleFilter} />
                            <FilterBar name="Filter by party" menu={parties} filterBy="party" onFilter={this.handleFilter} />
                            <FilterBar name="Filter by subject" menu={subjects} filterBy="subject" onFilter={this.handleFilter} />
                            <FilterBar className="sorting" name="Sort by" menu={sort} filterBy="sort" onFilter={this.handleFilter} />
                        </Grid>
                    </div>
                </div>
                </Col>
                </Row>
            </Container>
            </div>
            <Container>
                <div style={{ padding: 30 }}>
                    <Grid container spacing={24} justify="center">
                        {currentBills.map (item =>
                            (item.search_context === undefined || !this.url.includes("query=") ?
                    		<Grid item xs={12} sm={4} key={item.model_id}>
                                <BillCard
                                    item={item}
                                    key={item.model_id}
                                />
                            </Grid>:
                            <Grid item xs={12} key={item.model_id}>
                                    <ContextCard
                                        query={billQuery}
                                        item={item}
                                        key={item.model_id}
                                        image={item.bill_image}
                                        extension="/Bills/"
                                        cardName="bill-context-card"
                                        title={item.short_title}
                                    />

                            </Grid>))}
                    </Grid>
                    <br />
                    {totalBills !== 0 && (
                      <Pagination
                        className={classes.pagination}
                        currentPage={this.state.currentPage}
                        totalRecords={totalBills}
                        pageLimit={15}
                        onPageChanged={this.onPageChanged}
                      />
                    )}
                </div>
            </Container>
            </React.Fragment>
        );
    }
}


export default Bills;
