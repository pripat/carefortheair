import React, { Component } from "react";
import axios from 'axios';
import { FacebookProvider, Page } from 'react-facebook';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

import LocationCard from '../components/LocationCard.js';
import BillCard from '../components/BillCard.js'

const Header = {
  color: "black",
  fontSize: "40px",
  textAlign: "center"
};

class CharityInstance extends Component {
  constructor(props) {
    super(props)

    this.state = {
      charitiesID: this.props.match.params.id,
      attr: null,
      didLoad: false,
      relatedLocation: null,
      relatedBill: null
    }
  }

  async componentDidMount() {
    const getCharity = await axios.get('https://d3vnch75mm4o9r.cloudfront.net/charities/' + this.state.charitiesID);
    const getLocation = await axios.get('https://d3vnch75mm4o9r.cloudfront.net/locations/' + getCharity.data[0].related_location_id);
    const getBill = await axios.get('https://d3vnch75mm4o9r.cloudfront.net/bills/' + getCharity.data[0].related_bill_id);

    this.setState({
      attr: getCharity.data[0],
      didLoad: true,
      relatedLocation: getLocation.data[0],
      relatedBill: getBill.data[0]
    });
  }

  render() {
    if (this.state.didLoad) {
      const attr = this.state.attr;
      const charityName = attr.name;
      const charityPhoto = attr.charity_photo;
      const mission = attr.mission;
      const tagline = attr.tag_line;
      const cause = attr.cause;
      const foundStat = attr.foundation_status;
      const url = attr.website_url;
      const website = attr.name;
      const facebook = attr.facebook_username;

      const relatedLocation = this.state.relatedLocation;
      const relatedBill = this.state.relatedBill;

      return (
        <div>
            <br />
            <h1 style={Header} name="title"> {charityName} </h1>
            <div className="Content">
                <br />
                <Grid container direction="row" justify="center" alignItems="center" spacing={24}>
                    <Grid item xs={8} sm={6}>
                        <Card className="Content" class="card align-items-center">
                            <CardContent>
                              <img src={charityPhoto} style={{"width":"100%"}} border="0" alt="Null"/>
                              </CardContent>
                        </Card>
                        <br />
                          <Typography >
                              <b> Mission: </b> {mission} <br />
                          </Typography>
                          <Typography>
                              <b> Tagline: </b> {tagline} <br />
                          </Typography>
                          <Typography>
                              <b> Cause: </b> {cause} <br />
                          </Typography>
                          <Typography>
                              <b> Foundation Status: </b> {foundStat} <br />
                          </Typography>
                          <Typography>
                              <b> Charity URL: </b><a href={url}> {website} Website </a> <br />
                          </Typography>
                  </Grid>
                  <Grid item xs={8} sm={4}>
                    {facebook == null ?
                        (<FacebookProvider appId="2268884276498992">
                            <Page href={"https://www.facebook.com/freeandcleanair/"}
                            height="600"
                            width="500"
                            tabs="timeline" /></FacebookProvider>) :
                        (<FacebookProvider appId="2268884276498992">
                            <Page href={"https://www.facebook.com/" + facebook}
                            height="600"
                            width="500"
                            tabs="timeline, events" /></FacebookProvider>)}
                  </Grid>
                </Grid>
                <h1> </h1> <br />
                <h1> </h1> <br />
            </div>
            <div>
                <Grid container direction="row" justify="center" alignItems="baseline" spacing={24}>
                      <Grid item xs={8} sm={4} name='related-location'>
                          <Typography>
                              <b> Charity Location </b>
                          </Typography>
                               <LocationCard item={relatedLocation} />
                      </Grid>
                       <Grid item xs={8} sm={4} name='related-bill'>
                           <Typography>
                               <b> Related Bill </b>
                           </Typography>
                              <BillCard item={relatedBill} />
                      </Grid>
                 </Grid>
            </div>
            <footer className="page-footer font-small blue pt-4">
                <div className="footer-copyright text-center py-3">
                    © 2019 Copyright: Clouds. wow.
                </div>
            </footer>
       </div>
      );
    } else {
      return(<div></div>);
    }
  }
}

export default CharityInstance;
