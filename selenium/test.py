import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import random


class TestGUI(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()

    def test_hit_site(self):
        self.driver.get("https://carefortheair.me")
        self.assertIn("Care for the Air", self.driver.title)
        self.assertIn("carefortheair.me/Home", self.driver.current_url)

    def test_home_locations_card(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("locations-card").click()
        self.assertIn("carefortheair.me/Locations", self.driver.current_url)

    def test_home_bills_card(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("bills-card").click()
        self.assertIn("carefortheair.me/Bills", self.driver.current_url)

    def test_home_charities_card(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("charities-card").click()
        self.assertIn("carefortheair.me/Charities", self.driver.current_url)

    def test_home_nav(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("home-nav").click()
        self.assertIn("Care for the Air", self.driver.title)
        self.assertIn("carefortheair.me/Home", self.driver.current_url)

    def test_locations_nav(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("locations-nav").click()
        self.assertIn("carefortheair.me/Locations", self.driver.current_url)

    def test_bills_nav(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("bills-nav").click()
        self.assertIn("carefortheair.me/Bills", self.driver.current_url)

    def test_charities_nav(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("charities-nav").click()
        self.assertIn("carefortheair.me/Charities", self.driver.current_url)

    def test_search_nav(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("search-nav").click()
        self.assertIn("carefortheair.me/Search", self.driver.current_url)

    def test_about_nav(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("about-nav").click()
        self.assertIn("carefortheair.me/About", self.driver.current_url)

    def test_about_repo(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("about-nav").click()
        self.assertIn("carefortheair.me/About", self.driver.current_url)
        self.driver.find_element_by_link_text("GitLab Repo").click()
        self.assertIn(
            "https://gitlab.com/pripat/carefortheair", self.driver.current_url
        )

    def test_location_cards(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("locations-nav").click()
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("location-card")).click()
        self.assertIn("/Locations/", self.driver.current_url)
        time.sleep(3)
        self.assertEqual(len(self.driver.find_elements_by_name("title")), 1)
        self.driver.back()
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("location-card")).click()
        time.sleep(3)
        self.assertEqual(len(self.driver.find_elements_by_name("title")), 1)

    def test_bill_cards(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("bills-nav").click()
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("bill-card")).click()
        self.assertIn("/Bills/", self.driver.current_url)
        time.sleep(3)
        self.assertEqual(len(self.driver.find_elements_by_name("title")), 1)
        self.driver.back()
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("bill-card")).click()
        time.sleep(3)
        self.assertEqual(len(self.driver.find_elements_by_name("title")), 1)

    def test_charity_cards(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("charities-nav").click()
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("charity-card")).click()
        self.assertIn("/Charities/", self.driver.current_url)
        time.sleep(3)
        self.assertEqual(len(self.driver.find_elements_by_name("title")), 1)
        self.driver.back()
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("charity-card")).click()
        time.sleep(3)
        self.assertEqual(len(self.driver.find_elements_by_name("title")), 1)

    def test_about_page_cards(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("about-nav").click()
        time.sleep(3)
        self.assertEqual(len(self.driver.find_elements_by_name("user-card")), 5)
        for user_commits in self.driver.find_elements_by_name("commits"):
            self.assertTrue(int(user_commits.text.split(" ")[-1]) > 0)
        for user_issues in self.driver.find_elements_by_name("issues"):
            self.assertTrue(int(user_issues.text.split(" ")[-1]) > 0)

    def test_locations_related_models(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("locations-nav").click()
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("location-card")).click()
        time.sleep(3)
        self.driver.find_element_by_name("related-bill")
        self.driver.find_element_by_name("related-charity")

    def test_bills_related_models(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("bills-nav").click()
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("bill-card")).click()
        time.sleep(3)
        self.driver.find_element_by_name("related-location")
        self.driver.find_element_by_name("related-charity")

    def test_charities_related_models(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("charities-nav").click()
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("charity-card")).click()
        time.sleep(3)
        self.driver.find_element_by_name("related-location")
        self.driver.find_element_by_name("related-bill")

    def test_locations_pagination(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("locations-nav").click()
        time.sleep(3)
        self.driver.find_element_by_name("pagination")
        self.driver.find_element_by_name("first-page")
        self.driver.find_element_by_name("last-page")

    def test_bills_pagination(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("bills-nav").click()
        time.sleep(3)
        self.driver.find_element_by_name("pagination")
        self.driver.find_element_by_name("first-page")
        self.driver.find_element_by_name("last-page")

    def test_charities_pagination(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("charities-nav").click()
        time.sleep(3)
        self.driver.find_element_by_name("pagination")
        self.driver.find_element_by_name("first-page")
        self.driver.find_element_by_name("last-page")

    def test_locations_filter(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("locations-nav").click()
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("filter-menu")).click()
        random.choice(self.driver.find_elements_by_name("filter-item")).click()

    def test_bills_filter(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("bills-nav").click()
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("filter-menu")).click()
        random.choice(self.driver.find_elements_by_name("filter-item")).click()

    def test_charities_filter(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("charities-nav").click()
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("filter-menu")).click()
        random.choice(self.driver.find_elements_by_name("filter-item")).click()

    def test_locations_search(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("locations-nav").click()
        time.sleep(3)
        self.driver.find_element_by_name("search-input").send_keys("water")
        self.driver.find_element_by_name("search-button").click()
        self.assertIn("carefortheair.me/Location/Search", self.driver.current_url)
        time.sleep(3)
        random.choice(
            self.driver.find_elements_by_name("location-context-card")
        ).click()
        self.assertIn("/Locations/", self.driver.current_url)
        time.sleep(3)
        self.assertEqual(len(self.driver.find_elements_by_name("title")), 1)

    def test_bills_search(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("bills-nav").click()
        time.sleep(3)
        self.driver.find_element_by_name("search-input").send_keys("water")
        self.driver.find_element_by_name("search-button").click()
        self.assertIn("carefortheair.me/Bill/search", self.driver.current_url)
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("bill-context-card")).click()
        self.assertIn("/Bills/", self.driver.current_url)
        time.sleep(3)
        self.assertEqual(len(self.driver.find_elements_by_name("title")), 1)

    def test_charities_search(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("charities-nav").click()
        time.sleep(3)
        self.driver.find_element_by_name("search-input").send_keys("water")
        self.driver.find_element_by_name("search-button").click()
        self.assertIn("carefortheair.me/Charity/search", self.driver.current_url)
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("charity-context-card")).click()
        self.assertIn("/Charities/", self.driver.current_url)
        time.sleep(3)
        self.assertEqual(len(self.driver.find_elements_by_name("title")), 1)

    def test_site_wide_search_locations(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("search-nav").click()
        time.sleep(3)
        self.driver.find_element_by_name("search-input").send_keys("water")
        self.driver.find_element_by_name("search-button").click()
        self.driver.find_element_by_name("locations-tab").click()
        time.sleep(3)
        random.choice(
            self.driver.find_elements_by_name("location-context-card")
        ).click()
        self.assertIn("/Locations/", self.driver.current_url)
        time.sleep(3)
        self.assertEqual(len(self.driver.find_elements_by_name("title")), 1)

    def test_site_wide_search_bills(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("search-nav").click()
        time.sleep(3)
        self.driver.find_element_by_name("search-input").send_keys("water")
        self.driver.find_element_by_name("search-button").click()
        self.driver.find_element_by_name("bills-tab").click()
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("bill-context-card")).click()
        self.assertIn("/Bills/", self.driver.current_url)
        time.sleep(3)
        self.assertEqual(len(self.driver.find_elements_by_name("title")), 1)

    def test_site_wide_search_charities(self):
        self.driver.get("https://carefortheair.me")
        self.driver.find_element_by_name("search-nav").click()
        time.sleep(3)
        self.driver.find_element_by_name("search-input").send_keys("water")
        self.driver.find_element_by_name("search-button").click()
        self.driver.find_element_by_name("charities-tab").click()
        time.sleep(3)
        random.choice(self.driver.find_elements_by_name("charity-context-card")).click()
        self.assertIn("/Charities/", self.driver.current_url)
        time.sleep(3)
        self.assertEqual(len(self.driver.find_elements_by_name("title")), 1)

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
